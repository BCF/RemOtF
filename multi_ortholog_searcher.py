# coding: utf-8
#Orthologs searching for one protein

#Importing
import re, os, sys, subprocess, time, tempfile, shutil, multiprocessing
import math, operator, pickle, uuid
from Bio.Blast import NCBIXML
from optparse import OptionParser
from functions import u, error, parse_input_fasta, count_headers_fasta
##

#Global variables
cheader = re.compile('^>')
cseq = re.compile('^[ACDEFGHIKLMNPQRSTVWYUXJBZ\*]+$')
cblank = re.compile('^[\ \t]*$')
cerror1 = re.compile('^Selenocysteine')


#Processing command line arguments
usage = '%prog [options] -i INPUT_FILE -o OUTPUT_FILE'
parser = OptionParser(usage = usage)
parser.add_option('-i', metavar = "INPUT_FILE", dest = 'input_file', help =\
'Specify file to read.')
##
parser.add_option('-o', metavar = 'OUTPUT_DIR', dest = 'output_dir',
                  help = 'Directory for output FASTA with orthologs')

parser.add_option('-d', '--database', dest = 'database',
                  default = 'refseq_protein', help = 'BLAST database')

parser.add_option('-g', dest = 'gilist', default = '', help = 'Accessionlist file')

parser.add_option('-c', type = 'int', dest = 'cpus', default = 8,
                  help = 'Number of CPUs to use')

parser.add_option('-l', type = 'float', dest = 'coverage', default = 0,\
help = 'Mininmal length of the sequence found, [0, 200]')
##
parser.add_option('-m', type = 'float', dest = 'coverage_max', default = 200,\
help = 'Maximal length of the sequence found, [0, 200]')
##
parser.add_option('-q', type = 'int', dest = 'id_min', default = 0,\
help = 'Mininmal % identity in the alignment, [0, 100]')
##
parser.add_option('-f', type = 'int', dest = 'id_max', default = 100,\
help = 'Maximal % identity in the alignment, [0, 100]')

parser.add_option('-e', type = 'float', dest = 'evalue', default = 0.1,\
help = 'Maximal E-value')

parser.add_option('-p', type = 'int', dest = 'part', default = 0,\
help = 'which part of the proteome it is')

parser.add_option('--blast-xml', type = 'str', dest = 'blast_xml',\
help = 'A blast result in xml format to start directly the back_blast')

##
parser.add_option('-b', dest = 'input_accession', help = 'The accession\
        numbers of input species for faster back blast.')

parser.add_option('-s', dest = 'bblast_db', default = 'empty', help ="The blast database \
                  contening the input's proteome only. If the first blast db contain the \
                  input's proteome you can use -b to limit the back_blast to it.")

parser.add_option('--nobs', dest = 'name_ortho_by_species', default = None, help = "\
                  Name of the file where the number of ortholog by species are dump")

if __name__ == "__main__":
    if len(sys.argv) == 1:
       parser.print_help()
       sys.exit()
    (opt, args) = parser.parse_args()
    if not opt.input_file:
        error('Input file is not specified')
    if opt.input_file:
        opt.input_file = os.path.abspath(opt.input_file)
        if not os.path.isfile(opt.input_file):
            error('Input file does not exist')
    if opt.blast_xml and not os.path.isfile(opt.blast_xml):
        error('Input Blast.xml not exist')
    if opt.blast_xml and not opt.input_file:
        error('input_file needed with blast_xml')
    if not opt.output_dir:
        sys.stderr.write("Error: Output dir is not specified")
    opt.output_dir = os.path.abspath(opt.output_dir)
    if opt.gilist:
        if not os.path.isfile(opt.gilist):
            error('GI list file does not exist')
        opt.gilist = os.path.abspath(opt.gilist)
    if opt.cpus < 1:
        error('Number of CPUs must be a positive integen')
    if (opt.coverage > 200) or (opt.coverage < 0.0):
        error('Minimal coverage must be in range [0, 200]')
    if (opt.coverage_max > 200) or (opt.coverage_max < 0.0):
        error('Maximal coverage must be in range [0, 200]')
    if (opt.id_min > 100) or (opt.id_min < 0):
        error('Minimal % identity must be in range [0, 100]')
    if (opt.id_max > 100) or (opt.id_max < 0):
        error('Maximal % identity must be in range [0, 100]')

#Check for availability of executables
for binary in ['blastp', 'blastdbcmd']:
    found = None
    stdout, stderr = subprocess.Popen(['which', binary], stdout =\
    subprocess.PIPE, stderr = subprocess.PIPE).communicate()
    ##
    if not stdout:
        error('{} is not available\n'.format(binary))
        sys.exit(1)

#---Defining functions---
def blast_sorting(species):
    # used in: HMM_align.py
    """Function to sort ortholog lists and orphans"""
    Blast_output_path = species + 'bBlast_output/'
    if not os.path.exists(Blast_output_path + 'orphans/'):
        os.makedirs(Blast_output_path + 'orphans/')
    if not os.path.exists(Blast_output_path + 'identified/'):
        os.makedirs(Blast_output_path + 'identified/')

    fasta_to_sort = os.listdir(Blast_output_path)
    sortie = open('lists', 'a+')
    sortie.write(str(species) + ' | fasta_to_sort:\n' + str(fasta_to_sort) + '\n')
    sortie.close()
    for fastafile in fasta_to_sort:
        if os.path.isfile(
            os.path.join(
                Blast_output_path,
                fastafile)) and fastafile != 'number_ortholog_by_species' \
                and not fastafile.startswith('blast') and not fastafile.startswith('input_'):
            if count_headers_fasta(Blast_output_path, fastafile):
                shutil.move(
                    str(Blast_output_path) +
                    str(fastafile),
                    Blast_output_path +
                    'orphans/' +
                    fastafile)
            elif count_headers_fasta(Blast_output_path, fastafile) == False:
                shutil.move(
                    str(Blast_output_path) +
                    str(fastafile),
                    Blast_output_path +
                    'identified/' +
                    fastafile)

def trim_fasta_headers(input_file, max_len):
    """Function for trimming headers in a FASTA file"""
    tmp_interm = tempfile.NamedTemporaryFile('w+')
    with open(tmp_interm.name, 'w') as ofile:
        with open(input_file, 'r') as ifile:
            for line in ifile:
                if line.startswith('>') and len(line) > max_len:
                    line = line[0: max_len - 1] + '\n'
                ofile.write(line)
    shutil.copy(tmp_interm.name, input_file)
    tmp_interm.close()


def blastp(input_file, database, blast=False, evalue = '0.1',
                    num_alignments = '100', gilist = '', cpus = 8):
    """Function for running BLASTP and return an BLAST record object from NCBIXML"""
    if blast:
        tmp_blast = open(opt.output_dir+'/blastp_'+str(opt.part)+'.xml', 'w')
    else:
        tmp_blast = tempfile.NamedTemporaryFile('w+')

    with open(os.devnull, 'wb') as devnull:
        #print(f'cpus in blast_hit: {cpus}')
        check = subprocess.Popen(['blastp', '-db', database, '-query',\
        input_file, '-evalue', str(evalue), '-max_target_seqs', num_alignments,\
        '-outfmt', '5', '-out', tmp_blast.name, '-num_threads', str(cpus)] +\
        ['-seqidlist', gilist] * bool(gilist), stdout = devnull, stderr =\
        subprocess.PIPE) # Change -gilist to -seqidlist for test
        ##
        stdout, stderr = check.communicate()
        if stderr and (not cerror1.search(stderr.decode('utf-8'))):
            raise RuntimeError('Error in blastp: ' + u(stderr))
    results = NCBIXML.parse(open(tmp_blast.name))
    tmp_blast.close()
    return results

def parse_blast(blast_records,  coverage = 0, coverage_max = 200,
                    id_min = 0, id_max = 100, gilist = '', first_parse = False):
    """Function parsing the output of Blast record object, saving only the \
    best hit for each species"""

    #print(f'coverage {coverage}, coverage_max {coverage_max}')
    list_parse_result = []
    for result in blast_records:

        blast_hits = [[] for i in range(2)]

        alignments = len(result.alignments)
        len_ = result.query_length
        # Detect species of the query
        try:
        #print('result.query : ', result.query)
            species_query = re.search("\[(.*)\]", result.query)\
                    .group(1).replace(" ", "_")
        #print("species_query", species_query)
        except:
            continue
        for i in range(alignments):
            #print(result.alignments[i].title)
            #print(f'coverage {coverage}, coverage_max {coverage_max}')
            #print(' coverage   result.alignments[i].length  len_ length_align    coverage_max')
            #print(f" {len_*coverage*0.01}   {result.alignments[i].length}  {len_}   {result.alignments[i].length/len_*100}       {len_*coverage_max*0.01}")
            ident = result.alignments[i].hsps[0].identities
            headers = result.alignments[i].hit_def.split('>')
            headers[0] = result.alignments[i].hit_id + ' ' + headers[0]

            if first_parse == True:
                
                if (result.alignments[i].length < len_*coverage*0.01) \
                or (result.alignments[i].length >= len_ * coverage_max*0.01): # Ajout coverage_max
                    #print(' coverage   result.alignments[i].length  len_ length_align    coverage_max')
                    #print(f" {len_*coverage*0.01}   {result.alignments[i].length}  {len_}   {result.alignments[i].length}       {len_*coverage_max*0.01}")
                    #print(f"{result.alignments[i].title} rejected")
                    continue
                
                if (ident < len_*id_min*0.01) or\
                    (ident > len_*id_max*0.01):
                    #print(f"{result.alignments[i].title} rejected")
                    #print("ident:", ident, "len_:", len_)
                    #print('identities: ', ident)
                    #print("id_min: ", len_*id_min*0.01)
                    #print("id_max: ", len_*id_max*0.01)
                    continue
                #print(f"{result.alignments[i].title} passing")
                # if result.alignments[i].hsps[0].expect > opt.evalue:
                #     continue

            for header in headers:
                if 'partial' in header:
                    continue
                try:
                    gi = header.split("|")[1]
                    species = header.split('[')[1].split(']')[0]
                    if ' x ' in species:
                        continue
                    species = '{}_{}'.format(species.split()[0],
                                             species.split()[1])
                except IndexError:
                    continue
                # If the species found is the same of the query, pass
                #print("species :", species)
                if species == species_query:
                    continue
                    #print("blast_hits[0]", blast_hits[0])
                if species not in blast_hits[1]:
                    blast_hits[0].append(gi)
                    blast_hits[1].append(species)
        list_parse_result.append(blast_hits)
    return list_parse_result

def back_blast_worker(gi, species, query_species, self_gis, database, bblast_db, gilist,
                      cpus):
    """Function for back-BLASTing a single hit in a pool of processes"""
    tmp_query = tempfile.NamedTemporaryFile('w+')
    # Obtention of the sequence in the blast_db
    with open(os.devnull, 'wb') as devnull:
        check = subprocess.Popen(['blastdbcmd', '-db', database,\
        '-dbtype', 'prot', '-entry', gi, '-out', tmp_query.name], stdout =\
        devnull, stderr = subprocess.PIPE) #Retrieve sequence
        ##
        stdout, stderr = check.communicate()
        if stderr:
            if 'Entry not found' in u(stderr):
                tmp_query.close()
                return None
            return u(stderr)
    trim_fasta_headers(tmp_query.name, 1000)
    try: # Execute Blast
        res_blast = blastp(tmp_query.name, database = bblast_db, gilist = gilist,
                           cpus = cpus, evalue=opt.evalue)
        subhits = parse_blast(res_blast, id_max = 100, gilist = gilist)
        ##
    except Exception as e:
        return(str(e))

    # Get only the interesing part of the result of blast
    #print("subhits", subhits)
    subhits = subhits[0]
    #print("subhits[0]: ", subhits)
    #print("self_gis: ", self_gis)
    #print("query_species", query_species)
    # Look if the first hit correspond to the query accession number and species
    if subhits[1][0] == query_species and subhits[0][0] == self_gis:
        records = parse_input_fasta(tmp_query.name)
        seq = records[0][1]
        #print(records[0][0])
        tmp_query.close()
        #print(gi, species, seq)
        return gi, species, seq
    else:
        tmp_query.close()
        return None

def back_blast(blast_hits, self_gis, bblast_db, query_species,
               database = 'refseq_protein', gilist = '', cpus = 8):
    """Function for performing back-BLAST"""
    def append_result(result):
        """Closure to process individual back-BLASTing results"""
        if type(result) == str:
            error(result)
        if result:
            bblast_hits[0].append(result[0])
            bblast_hits[1].append(result[1])
            bblast_hits[2].append(result[2])
    bblast_hits = [[] for i in range(3)]
    len_ = len(blast_hits[0])
    cpus = math.ceil(cpus / max(len_, 1))
    pool = multiprocessing.Pool()
    #print(f'cpus in back_blast = {cpus}')
    for i in range(len_):
        pool.apply_async(back_blast_worker, args = (blast_hits[0][i],\
                        blast_hits[1][i], query_species, self_gis, database,\
                        bblast_db, gilist, cpus), callback = append_result)
    pool.close()
    pool.join()
    return bblast_hits

#get sequences and write fasta file---Orthologs of query seq
def write_output(bblast_hits, output_file, query_id, query_seq):
    """Function for writing the results to file"""
    with open(output_file, 'w') as file:
        file.write('>0_{}\n{}\n'.format(query_id, query_seq))
        for i in range(len(bblast_hits[0])):
            # delete input sequence if it's have been find
            if bblast_hits[2][i] == query_seq:
                continue
            file.write('>{}_{} gi|{}\n{}\n'.format(i + 1,\
            bblast_hits[1][i].replace(' ', '_'), bblast_hits[0][i],\
            bblast_hits[2][i]))
            ##

def count_and_save_orthologs_by_species(list_bblast_hits, name_ortho_by_species):
    '''Count the number of ortholog found by species and dumb the result in temp/n_orthos_by_species'''
    dict_ortholog_species = {}
    for bblast in list_bblast_hits:
        for bb_species in bblast[1]:
            if bb_species in list(dict_ortholog_species.keys()):
                dict_ortholog_species[bb_species] += 1
            else:
                dict_ortholog_species[bb_species] = 1
    # Sort the result
    sorted_ortholog_species = sorted(dict_ortholog_species.items(), key=operator.itemgetter(1),\
                                     reverse = True)
    # Save the number of ortholog by species in a file
    pickle.dump(sorted_ortholog_species, \
            open(f"{os.getcwd()}/temp/n_ortho_by_species/{opt.name_ortho_by_species}", "wb"))

#---Main section---
starttime = int(time.time())
# If no bblast_db are given, use the first blast database
if opt.bblast_db == "empty":
    opt.bblast_db = opt.database
if opt.input_accession == "empty":
    opt.input_accession = None

list_bblast_hits = []
pwd = os.getcwd()
c = 0

print(f"blastdb : {opt.database}")
print(f"bblastdb : {opt.bblast_db}")
if opt.input_file and not opt.blast_xml:
    print("Processing {}".format(opt.input_file.rsplit('/', 1)[1]))
    print('Checking input... ', end = '')
    sys.stdout.flush()
    records = parse_input_fasta(opt.input_file)
#print("len(records):", len(records))
    print('Done.')
    len_input = len(records)
    print(f'Blasting the {len_input} sequence')

    blast_results = blastp(opt.input_file, opt.database, gilist = opt.gilist,
                     cpus = opt.cpus, evalue = opt.evalue, blast=True)
    time_for_blast = int(time.time()) - starttime
    print(f"End of the blastp, take: {time_for_blast} sec")
else:
    records = parse_input_fasta(opt.input_file)
    blast_results =  NCBIXML.parse(open(opt.blast_xml))

# Parse the result
#print(f"before parse : coverage {opt.coverage}, coverage_max {opt.coverage_max}")
l_blast_hit = parse_blast(blast_results,  coverage = opt.coverage, coverage_max = opt.coverage_max,
                id_min = opt.id_min, id_max = opt.id_max,
                gilist = opt.gilist, first_parse = True)

# print("l_blast_hit: ", l_blast_hit)
time_for_parsing = int(time.time()) - starttime
print(f"End of the parsing, take: {time_for_parsing} sec")
# Take the result of the blast and parse one by one
for blast_hit in l_blast_hit:
    #print(blast_hit, "\n")
    #print(f'blast_hit[0][0]: {blast_hit[0][0]}\n blast_hit[0][1]: {blast_hit[0][1]}')
    #print(len(blast_hit[0][0]), len(blast_hit[0][1]))
    #print(records[c][0])
    try:
        species_record = re.search("\[(.*)\]", records[c][0]).group(1).replace(" ", "_")
        accession_record = re.search(">([^ ]+)", records[c][0]).group(1)
        #print(species_record)
        #print(accession_record)
    except:
        print("Didn't find the species or accession record")
        continue
    print('accession_record', accession_record)
    before_bblast_start = time.time()
    # Do the back blast
    
    bblast_hits = back_blast(blast_hit, accession_record, opt.bblast_db, species_record,
                             database = opt.database, gilist = opt.input_accession,
                             cpus = opt.cpus) #Remplacement of gilist_species
    #print(bblast_hits)
    print('Done with back-blasting. There are {} supposed orthologs'.\
    format(len(bblast_hits[2])))
    bblast_hits_time = int(time.time() - before_bblast_start)
    print(f'BBlast_hits done in: {bblast_hits_time} sec')
    # Save the backblast result for the count of ortholog by species
    list_bblast_hits.append(bblast_hits)
    print('Saving results into file...')

    write_output(bblast_hits, opt.output_dir+'/'+accession_record+'_ortho.fa', records[c][0], records[c][1])
    print('Done with searching for orthologs of {}.'.\
    format(accession_record))

    c += 1

# Count the number of ortholog by species
if opt.name_ortho_by_species != None:
    count_and_save_orthologs_by_species(list_bblast_hits, opt.name_ortho_by_species)

# Filtering the result in identified and orphan
    before_filtering = int(time.time())
    blast_sorting(opt.output_dir.replace("bBlast_output", ""))
    print(f"Filtering take {int(time.time()) - before_filtering} sec")


print('Runtime: {} sec'.format(int(time.time() - starttime)))
