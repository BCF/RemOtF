import os
import sys
import argparse
from functions import error
from operator import itemgetter

parser = argparse.ArgumentParser(description="Creation of hhsuite database who WORK")
required = parser.add_argument_group('required arguments')
required.add_argument("--a3m", nargs="+", metavar="", help="Glob of a3m file", required=True)
required.add_argument("--hhm", nargs="+", metavar="", required=True,  help="Glob of hhm file (need to have the same name of the a3m file)")
required.add_argument("--cs219", default=False, action="store_true", required=True)
required.add_argument("-d", "--database", required=True, help="Name of the hhsuite database")
parser.add_argument("-c", "--cpus", type=int, default=1, required=True, help="Number of cpus available")
args = parser.parse_args()


def calculate_cs219(threads, a3m_db_base, cs_db_base):
    """Call cstranslate to create the cs219 of the db"""
    try:
        HHLIB = os.environ['HHLIB']
    except:
        error("ERROR: HHLIB environment variable not set! See manual!\n")

    os.putenv("OMP_NUM_THREADS", str(threads))
    os.system(f"cstranslate -A {HHLIB}/data/cs219.lib"
        + f" -D {HHLIB}/data/context_data.lib -f -i {a3m_db_base} -o {cs_db_base} -I a3m -x 0.3 -c 4")


if __name__ == '__main__':

    # Initialisation of variable
    c_lenght = 0
    c_lenght_hhm = 0
    basename_a3m = []
    # Check that hhm and a3m have the same name
    for filename in args.a3m:
        basename_a3m.append(os.path.basename(filename))
    for filename in args.hhm:
            if os.path.basename(filename).replace('.hhm', '.a3m') not in basename_a3m:
                error('HHM not in a3m')

    # Create the a3m part of the database
    a3m_data = open(args.database+'_a3m.ffdata', 'w')
    a3m_index = open(args.database+'_a3m.ffindex', 'w')
    for filename in args.a3m:
        file = open(filename, 'r').read()
        a3m_data.write(file+'\0')
        a3m_index.write(f"{os.path.basename(filename)}\t{c_lenght}\t{len(file)+1}\n")
        c_lenght += len(file)+1

    # Create the hhm part of the database
    hhm_data = open(args.database+'_hhm.ffdata', 'w')
    hhm_index = open(args.database+'_hhm.ffindex', 'w')
    for filename in args.hhm:
        file = open(filename, 'r').read()
        hhm_data.write(file+'\0')
        hhm_index.write(f"{os.path.basename(filename)}\t{c_lenght_hhm}\t{len(file)+1}\n")
        c_lenght_hhm += len(file)+1

    # Write imediatly the a3m file so cstranslate can use them
    a3m_data.flush()
    a3m_index.flush()

    # Launch creation of cs219 part of the database
    calculate_cs219(args.cpus, args.database+'_a3m', args.database+'_cs219')
