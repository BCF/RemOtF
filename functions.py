#!/usr/bin/python3
# coding: utf-8

import os
import sys
import re
import time
import math
import signal
import shutil
import os.path
import operator
import subprocess
from Bio import SeqIO


'''
Regex lexicon for hhalignments: Designed using https://pythex.org/
UPDATED 08 FEB 2019
Query Accession Number: Query.*>([A-Za-z0-9._-]*) 
Query function: Query.*>[A-Za-z0-9._-]* *(.*)\[.*\] 
Query species with []: Query.*>.*(\[.*\])
Template Accession Number: No \d+\n.*>([A-Za-z0-9._-]*)
Template function: No \d+\n.*>[A-Za-z0-9._-]* *(.*)\[.*\]
Template species with []: No \d+\n.*>.*(\[.*\])
qu_start and qu_end: \nQ (?!Consensus)>[0-9a-zA-Z._-]* *([0-9]*) *[A-Z-]* *([0-9]*)
Query length: \nQ (?!Consensus)>[0-9a-zA-Z._-]*.*\(([0-9]*)\)
te_start and te_end: \nT (?!Consensus)>[0-9a-zA-Z._-]* *([0-9]*) *[A-Z-]* *([0-9]*)
Template length: \nT (?!Consensus)>[0-9a-zA-Z._-]*.*\(([0-9]*)\)
/!\ if the alignment is too long to fit on one line
    or if there are multiple found alignments,
    the above regexes will take all start and end values in it.

Probab: Probab\=([0-9\.]*)
E-value: E\-value\=([0-9\.]*)
Score: Score\=([0-9\.]*)
Aligned columns: Aligned_cols\=([0-9\.]*)
Identities: Identities\=([0-9\.]*)%
Similarity: Similarity\=([0-9\.-]*)
Sum of probabilities: Sum_probs\=([0-9\.]*)
Template Neff: Template_Neff\=([0-9\.]*)

ADDED PREVIOUSLY
Query path: Command.*-i (.*) -d
Template path: Command.*-d (.*) -o
ADDED 08 FEB 2019
Template Accession Number from alignment line: \nT (?!Consensus)([>0-9a-zA-Z._-]*)

to implement (maybe not anymore?):
Template species without []: >0_>.*\[(.*)\]

Date *(.*)

to test:
Q 0_>[0-9a-zA-Z._]* *[0-9]* *([A-Z-]*) *[0-9]*
Q Consensus *[0-9]* *([A-Za-z-~]*) *[0-9]*
T 0_>[0-9a-zA-Z._]* *[0-9]* *([A-Z-]*) *[0-9]*
T Consensus *[0-9]* *([A-Za-z-~]*) *[0-9]*
for characters in alignments: \s{23}([\W]*)
'''

# ---Declaring classes
# ============================================================================


class HHresult:
    # used in: HH_align.py
    '''Class containing all the hhalign results'''
    # ------------------------------------------------------------------------

    def __init__(self,
                 qu_AN, qu_function, qu_species,
                 te_AN, te_function, te_species,
                 qu_start, qu_end, qu_length,
                 te_start, te_end, te_length,
                 prob, evalue, score, al_col, ident,
                 simil, sum_probs, te_neff,
                 qu_path, te_path):
        self.qu_AN = str(qu_AN)  		# Query Accession Number
        self.qu_function = str(qu_function)  	# Query Annotated Function
        self.qu_species = str(qu_species)  	# Query Species
        self.te_AN = str(te_AN)  		# Template Accession Number
        self.te_function = str(te_function)  	# Template Annotated Function
        self.te_species = str(te_species)  	# Template Species
        self.qu_start = int(qu_start)  		# Beginning of alignment on query
        self.qu_end = int(qu_end)  		# End of alignment on query
        self.qu_length = str(qu_length)  	# Total length of query
        self.te_start = int(te_start)  		# Beginning of alignment on template
        self.te_end = int(te_end)  		# End of alignment on template
        self.te_length = str(te_length)  	# Total length of template
        self.prob = float(prob)  		# Probability
        self.evalue = float(evalue)  		# E-value
        self.score = float(score)  		# Score
        self.al_col = int(al_col)  		# Number of aligned columns
        self.ident = float(ident)  		# Identity
        self.simil = float(simil)  		# Similarity
        self.sum_probs = float(sum_probs)  	# Sum of probabilities
        self.te_neff = str(te_neff)             #
        self.qu_path = str(qu_path)  		# relative path to the query file
        self.te_path = str(te_path)  		# relative path to the template file

    def __str__(self):
        return('Query: ' + ' ' + self.qu_AN + ' ' + self.qu_function + ' ' + self.qu_species + '\n'
               + 'Template: ' + ' ' + self.te_AN + ' ' + self.te_function + ' ' + self.te_species + '\n'
               + 'Query start: ' + str(self.qu_start) + '\n'
               + 'Query end: ' + str(self.qu_end) + '\n'
               + 'Query length: ' + str(self.qu_length) + '\n'
               + 'Template start: ' + str(self.te_start) + '\n'
               + 'Template end: ' + str(self.te_end) + '\n'
               + 'Template length: ' + str(self.te_length) + '\n'
               + 'Probability: ' + str(self.prob) + '\n'
               + 'E-value: ' + str(self.evalue) + '\n'
               + 'Score: ' + str(self.score) + '\n'
               + 'Aligned columns: ' + str(self.al_col) + '\n'
               + 'Identity: ' + str(self.ident) + '%\n'
               + 'Similarity: ' + str(self.simil) + '\n'
               + 'Sum of probabilities: ' + str(self.sum_probs) + '\n'
               + 'Template Neff.: ' + str(self.te_neff) + '\n'
               + 'query path: ' + str(self.qu_path) + '\n'
               + 'template path: ' + str(self.te_path))

    def qu_align_length(self):
        return(self.qu_end - self.qu_start)

    def te_align_length(self):
        return(self.te_end - self.te_start)
 # ------------------------------------------------------------------------
# ============================================================================

# ---Declaring functions---


def error(message, specification=''):
    # used in: multiple_ortholog_searcher.py
    """Function for error reporting"""
    sys.stderr.write('Error{}: {}\n'.format(specification, message))
    sys.stderr.flush()
    time.sleep(1)
    parent = os.popen('ps -p {}'.format(os.getppid())).read().split()[-1]
    gparent_pid = os.popen("ps -p {} -oppid=".format(os.getppid())).read().\
        strip()
    ##
    gparent = os.popen('ps -p {}'.format(gparent_pid)).read().split()[-1]
    if parent == 'mpiexec':
        from mpi4py import MPI
        MPI.COMM_WORLD.Abort(1)
    elif gparent == 'mpiexec':
        os.kill(os.getppid(), signal.SIGTERM)
    else:
        sys.exit(1)


def u(byte_string):
    # used in: multiple_ortholog_searcher.py
    """Function for decoding byte strings as unicode"""
    return byte_string.decode('utf-8')


def parse_input_fasta(fasta):
    # used in: main.py, multiple_ortholog_searcher.py
    '''Parse the fasta file given and return it as a list of list'''
    list_fasta = []
    records = SeqIO.parse(fasta, 'fasta')
    for record in records:
        list_fasta.append(['>' + record.description, str(record.seq)])
    return list_fasta


def get_input_fasta_first_record(input_file, loose=False):
    # used in: check_input_fasta()
    """Function for the first record from the input FASTA file"""
    cheader = re.compile('^>')
    if loose:
        cseq = re.compile('^[^>]')
    else:
        cseq = re.compile('^[ABCDEFGHIKLMNPQRSTVWYUXJBZ-]+\*?$')
    cblank = re.compile('^[\ \t]*$')
    first = True
    expect_seq = False
    err = False
    header = ''
    seq = ''
    with open(input_file, 'r') as file:
        for line in file:
            if cheader.search(line):
                if expect_seq:
                    first = True
                if first:
                    first = False
                else:
                    break
                header = line.strip()
                seq = ''
                expect_seq = True
            elif cseq.search(line):
                seq += line.strip()
                expect_seq = False
            elif cblank.search(line):
                pass
            else:
                err = True
                break
        record = [header, seq]
        if err:
            raise IOError('Input file {} is not valid'.format(input_file))
    return record


def select_own_file(path, number_nodes, which_node):
    # used in: HMM_build.py, HMM_align.py
    '''Return the list of file corresponding to the node'''
    if int(number_nodes) == 0:
        listdir = os.listdir(path)
        return listdir
    else:
        j = 0
        dir_by_node = []
        listdir = os.listdir(path)
        #listdir = listdir.sort()
        print(listdir)
        number_by_node = math.ceil(len(listdir) / int(number_nodes))

        for node in range(int(number_nodes)):
            try:
                dir_by_node.append(listdir[j:j + int(number_by_node)])
            except IndexError:
                try:
                    dir_by_node.append(
                        [listdir[j:j + int(number_by_node) - 1]])
                except BaseException:
                    print('ça bug quand meme')
            j += number_by_node
        return dir_by_node[int(which_node)]


def check_binaries(binary_list):
    # used in: main.py
    """Function for checking the availability of required binaries"""
    for binary in binary_list:
        stdout, stderr = subprocess.Popen(
            ['which', binary], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
        ##
        if (not stdout) or (not stdout.startswith(b'/')):
            error('{} is not available\n'.format(binary))


def hmm_builder(input_dir, input_file, mafft_output_dir, hh_output_dir):
    # used in: HMM_build.py
    '''/!\ Doing only the mafft and the conversion to a3m as the hhsuitedb is create weirdly'''
# Making Hidden Markov Models in 3 steps:
# 1/ Checking for validity
    # print('hmm_builder: Processing ' + input_file)
    input_path = str(input_dir) + str(input_file)
    check_input_fasta(input_path)
    seq = get_input_fasta_first_record(input_path)[1]
    len_ = len(seq)
    if len_ > 32767:
        raise ValueError('Protein is too long: {} residues (max. 32767)'.
                         format(len_))
# 2/ For each ortholog list, align sequences via MAFFT
    mafft_output = mafft_output_dir + str(input_file).replace('.fa', '_msa.fa')
    os.system(
        'mafft --maxiterate 1000 --globalpair ' +
        input_path +
        ' > ' +
        mafft_output)
    if os.path.isfile(mafft_output):
        print('Saved in ' + mafft_output)
        os.system(f"reformat.pl fas a3m '{mafft_output}' .a3m")
    else:
        error('MAFFT output didn\'t write correctly')

# UNCOMMENT this code when hhsuitedb.py work correctly
# 3/ Each MSA is then used to make HMMs
    hh_output = hh_output_dir + str(input_file).replace('.fa', '.hhm')
    try:
        stdout, stderr = subprocess.Popen(['hhmake', '-i', mafft_output,
            '-M', 'first', '-o', hh_output],
            stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    except Exception as e:
        error("hhmake stderr: ", stderr)

    if os.path.isfile(hh_output):
        print('Saved in ' + hh_output)
    else:
        error("hhmake stderr: ", stderr)

def hmm_builder_orphans(input_dir, input_file, hh_output_dir):
    # used in: HMM_build.py
    '''Creates HMM from one sequence files'''
# 1/ Checking for validity
    # print('hmm_builder: Processing ' + input_file)
    input_path = str(input_dir) + str(input_file)
    check_input_fasta(input_path)
    seq = get_input_fasta_first_record(input_path)[1]
    len_ = len(seq)
    if len_ > 32767:
        raise ValueError('Protein is too long: {} residues (max. 32767)'.
                         format(len_))
# 2/ Each orphan is then used to make HMM
    if not input_dir.endswith("back_hmm_library/"):
        hh_output = str(hh_output_dir) + str(input_file).replace('.fa', '.hhm')
        try:
            stdout, stderr = subprocess.Popen(['hhmake', '-i', input_path,
                '-M', 'first', '-o', hh_output],
                stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
        except Exception as e:
            error("hhmake stderr: ", stderr)
        if os.path.isfile(hh_output):
            print('HMM built')
            print('Saved in ' + hh_output)
        else:
            #error('HHmake didn\'t write ' + hh_output + ' correctly')
            error("hhmake stderr: ", stderr)
    # Convert fasta to a3m for hhsuitedb.py
    os.system(f"reformat.pl fas a3m '{input_path}' .a3m")

def extract_file_from_hhdb(id, db_path, output_path):
    """Extract an hhm file from a hhsuitedb to the output_path and return the path of the file"""
    # stdout = subprocess.Popen(['ffindex_get', db_name+'_hmm.ffdata',
    #     db_name+'_hmm.ffindex', id.replace('0_', '')+"_ortho.a3m", '>',
    #     run_path+'back_align/'+id.replace('0_', '')+"_ortho.hhm"],
    #     stdout = subprocess.PIPE).communicate()
    os.system(f"ffindex_get {db_path}_a3m.ffdata \
        {db_path}_a3m.ffindex {id.replace('0_', '')}_ortho.a3m > \
        {output_path+id.replace('0_', '')+'_ortho.hhm'}")
    # print(f"ffindex_get {db_path}_hhm.ffdata \
    #     {db_path}_hhm.ffindex {id.replace('0_', '')}_ortho.a3m > \
    #     {output_path+id.replace('0_', '')+'_ortho.hhm'}")

    return output_path+id.replace('0_', '')+"_ortho.hhm"

def hmm_align(queries_path, query, templates_dir_path, output_path):
    # used in: HMM_align.py
    '''makes HMM alignments between query species and selected ones.'''
    if '_ortho.hmm' in query:
        output_dir = output_path + query.replace('_ortho.hhm', '/')
    else:
        output_dir = output_path + query.replace('.hhm', '/')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    # print(templates_dir_path)

    for templates_path in templates_dir_path:
        templates_list = os.listdir(templates_path)
        for template in templates_list:
            if '_ortho.hmm' in query:
                align_output = output_dir + query.replace('ortho.hhm', '')
                if '_ortho.hmm' in template:
                    align_output += template.replace('_ortho.hhm', '.hhr')
                else:
                    align_output += template.replace('.hhm', '.hhr')
            else:
                align_output = output_dir + query.replace('ortho.hhm', '')
                if '_ortho.hmm' in template:
                    align_output += template.replace('_ortho.hhm', '.hhr')
                else:
                    align_output += template.replace('.hhm', '.hhr')
            os.system('hhalign -i ' + queries_path + query + ' -t ' +
                      templates_path + template + ' -o ' + align_output)


def check_input_fasta(input_file, align=False):
    # used in: hmm_builder()
    """Function for checking for validity input FASTA file"""
    # print('check_input_fasta: Processing ' + input_file)
    with open(input_file, 'r') as file:
        err = False
        first_line = True
        line_prev = ''
        header = re.compile('^>')
        if align:
            record = re.compile('^[A-Z\-]+\*?$')
        else:
            record = re.compile('^[A-Z]+\*?$')
        index = 0
        for line in file:
            index += 1
            match_re = False
            if header.search(line):
                match_re = True
                if first_line:
                    first_line = False
                if header.search(line_prev):
                    err = True
            if record.search(line):
                match_re = True
                if line_prev.isspace():
                    err = True
            if line.isspace():
                match_re = True
                if header.search(line_prev):
                    err = True
            if not match_re:
                err = True
            if err:
                raise IOError('Input file {} is corrupt or not FASTA. Error wh\
ile parsing line {}'.format(input_file.split('/')[-1], index))
                sys.exit(1)
                ##
            line_prev = line
    # print('check_input_fasta: Finishing ' + input_file)


def blast_sorting(species):
    # used in: HMM_align.py
    """Function to sort ortholog lists and orphans"""
    Blast_output_path = species + 'bBlast_output/'
    if not os.path.exists(Blast_output_path + 'orphans/'):
        os.makedirs(Blast_output_path + 'orphans/')
    if not os.path.exists(Blast_output_path + 'identified/'):
        os.makedirs(Blast_output_path + 'identified/')

    fasta_to_sort = os.listdir(Blast_output_path)
    for fastafile in fasta_to_sort:
        if os.path.isfile(
            os.path.join(
                Blast_output_path,
                fastafile)) and fastafile != 'number_ortholog_by_species':
            if count_headers_fasta(Blast_output_path, fastafile):
                shutil.move(
                    str(Blast_output_path) +
                    str(fastafile),
                    Blast_output_path +
                    'orphans/' +
                    fastafile)
            elif count_headers_fasta(Blast_output_path, fastafile) == False:
                shutil.move(
                    str(Blast_output_path) +
                    str(fastafile),
                    Blast_output_path +
                    'identified/' +
                    fastafile)


def get_input_fasta_first_record(input_file, loose=False):
    # used in: hmm_builder()
    """Function for the first record from the input FASTA file"""
    # print('get_input_fasta_first_record: Processing ' + input_file)
    cheader = re.compile('^>')
    if loose:
        cseq = re.compile('^[^>]')
    else:
        cseq = re.compile('^[ABCDEFGHIKLMNPQRSTVWYUXJBZ-]+\*?$')
    cblank = re.compile('^[\ \t]*$')
    first = True
    expect_seq = False
    err = False
    header = ''
    seq = ''
    with open(input_file, 'r') as file:
        for line in file:
            if cheader.search(line):
                if expect_seq:
                    first = True
                if first:
                    first = False
                else:
                    break
                header = line.strip()
                seq = ''
                expect_seq = True
            elif cseq.search(line):
                seq += line.strip()
                expect_seq = False
            elif cblank.search(line):
                pass
            else:
                err = True
                break
        record = [header, seq]
        if err:
            raise IOError('Input file {} is not valid'.format(input_file))
    # print('get_input_fasta_first_record: Finishing ' + input_file)
    return record


def count_headers_fasta(input_dir, input_file):
    # used in: blast_sorting()
    '''Function to parse and count headers in a fasta'''
    input_path = str(input_dir) + str(input_file)
    with open(input_path, 'r') as file:
        header = re.compile('^>')
        index = 0
        for line in file:
            if header.search(line):
                index += 1
        if index == 1:
            return True
        elif index > 1:
            return False
        else:
            raise IOError('Input file {} is corrupt or not FASTA. Error wh\
            ile parsing line {}'.format(input_path.split('/')[-1], index))


def parse_hh_align(input_hhr, align=True):
    # used in: HMM_align.py
    '''Function to parse hhsearch/hhblits result to gather query and 
    first hit info into HHresult class'''

    qu_AN_re = re.compile('Query.*>([A-Z0-9_-]+[0-9.]+)')
    qu_function_re = re.compile('Query.*[>A-Z0-9_-]+[0-9.]+\.\d* *(.*)\[.*\]')
    qu_species_re = re.compile('Query.*>.*(\[.*\])')
    te_AN_re = re.compile('No \d+\n.*?([>A-Z0-9_-]+[0-9.]+)')
    te_function_re = re.compile('No \d+\n.*?[>A-Z0-9_-]+[0-9.]+ *(.*)\[.*\]')
    te_species_re = re.compile('No \d+\n.*?(\[.*\])')
    qu_start_end_re = re.compile(
        '\nQ (?!Consensus)[>0-9a-zA-Z._-]* *([0-9]*) *[A-Z-]* *([0-9]*)')
    qu_length_re = re.compile('\nQ (?!Consensus)[>0-9a-zA-Z._-]*.*\(([0-9]*)\)')
    te_start_end_re = re.compile(
        '\nT (?!Consensus)[>0-9a-zA-Z._-]* *([0-9]*) *[A-Z-]* *([0-9]*)')
    te_length_re = re.compile('\nT (?!Consensus)[>0-9a-zA-Z._-]*.*\(([0-9]*)\)')
    prob_re = re.compile('Probab\=([0-9\.]*)')
    evalue_re = re.compile('E\-value\=([0-9\.]*[e+-]*[0-9]*)')
    score_re = re.compile('Score\=([0-9\.]*[e-]*[0-9]*)')
    al_col_re = re.compile('Aligned_cols\=([0-9\.]*)')
    ident_re = re.compile('Identities\=([0-9\.]*)%')
    simil_re = re.compile('Similarity\=([0-9\.-]*)')
    sum_probs_re = re.compile('Sum_probs\=([0-9\.]*)')
    te_neff_re = re.compile('Template_Neff\=([0-9\.]*)')
    qu_path_re = re.compile('Command.*-i (.*) -d')
    te_path_re = re.compile('Command.*-d (.*) -o')
    te_AN_align_re = re.compile('\nT (?!Consensus)([>0-9a-zA-Z._-]*)')

    # Open and read all the hhm file:
    HH_res_list = []
    i = 1
    file = open(input_hhr, 'r')
    readfile = file.read()
    file.close()

# find all the matches
    qu_AN_matches = qu_AN_re.findall(readfile)
    qu_function_matches = qu_function_re.findall(readfile)
    qu_species_matches = qu_species_re.findall(readfile)
    te_AN_matches = te_AN_re.findall(readfile)
    te_function_matches = te_function_re.findall(readfile)
    te_species_matches = te_species_re.findall(readfile)
# Start and end are found unpaired, so it is needed to rearrange them
    qu_start_end_matches_raw = qu_start_end_re.findall(readfile)
    qu_start_end_matches = []
    for x in qu_start_end_matches_raw:
        if x[0] and x[1]:
            qu_start_end_matches.append(int(x[0]))
            qu_start_end_matches.append(int(x[1]))
    qu_length_matches = qu_length_re.findall(readfile)
    te_start_end_matches_raw = te_start_end_re.findall(readfile)
    te_start_end_matches = []
# Same as before
    for x in te_start_end_matches_raw:
        if x[0] and x[1]:
            te_start_end_matches.append(int(x[0]))
            te_start_end_matches.append(int(x[1]))
    te_length_matches = te_length_re.findall(readfile)
    prob_matches = prob_re.findall(readfile)
    evalue_matches = evalue_re.findall(readfile)
    score_matches = score_re.findall(readfile)
    al_col_matches = al_col_re.findall(readfile)
    ident_matches = ident_re.findall(readfile)
    simil_matches = simil_re.findall(readfile)
    sum_probs_matches = sum_probs_re.findall(readfile)
    te_neff_matches = te_neff_re.findall(readfile)
    qu_path_matches = qu_path_re.findall(readfile)
    te_path_matches = te_path_re.findall(readfile)
    te_AN_align_matches = te_AN_align_re.findall(readfile)

    print(qu_path_matches)
    # print(te_path_matches)
    print('accession: ', qu_AN_matches, te_AN_matches, qu_start_end_matches)
    # print('fonction: ', qu_function_matches, te_function_matches)
    # print("species: ", qu_species_matches, te_species_matches)

    # Number of couples start-end to check if contigue
#    print(input_hhr + ' CHECK TURNS')
    turns = int(len(qu_start_end_matches) / 2)
#    print(input_hhr + ' turns = ' + str(turns) + '\n')

    # Concatenate long spanning alignments and separate into specific start and end position lists
    qu_start_matches = []
    qu_end_matches = []
    te_start_matches = []
    te_end_matches = []
    while turns != 0 and turns > 1:
        if (qu_start_end_matches[-2] == qu_start_end_matches[-3] + 1) \
        and (te_AN_align_matches[-1] == te_AN_align_matches[-2]):
            del qu_start_end_matches[-3]
            del qu_start_end_matches[-2]
            del te_start_end_matches[-3]
            del te_start_end_matches[-2]
            del te_AN_align_matches[-1]
            turns -= 1
        else:
            qu_start_matches = [qu_start_end_matches[-2]] + qu_start_matches
            qu_end_matches = [qu_start_end_matches[-1]] + qu_end_matches
            te_start_matches = [te_start_end_matches[-2]] + te_start_matches
            te_end_matches = [te_start_end_matches[-1]] + te_end_matches
            del qu_start_end_matches[-2]
            del qu_start_end_matches[-1]
            del te_start_end_matches[-2]
            del te_start_end_matches[-1]
            del te_AN_align_matches[-1]
            turns -= 1
    if turns == 1:
        qu_start_matches = [qu_start_end_matches[0]] + qu_start_matches
        qu_end_matches = [qu_start_end_matches[1]] + qu_end_matches
        te_start_matches = [te_start_end_matches[0]] + te_start_matches
        te_end_matches = [te_start_end_matches[1]] + te_end_matches

    # Select the best hit for each species and saving
    best_hit_species = []
    turns = len(te_AN_matches)
    for turn in range(turns):
        print(qu_AN_matches[0])
        print(str(turn) + '     ' + te_AN_matches[turn] + te_species_matches[turn] + '\n')
        if not te_species_matches[turn] in best_hit_species:
            best_hit_species.append(te_species_matches[turn])
            HH_name = qu_AN_matches[0] + '_' + te_AN_matches[turn]
            if len(qu_path_matches) == 1:
                HH_name = HHresult(qu_AN=qu_AN_matches[0],
                               qu_function=qu_function_matches[0],
                               qu_species=qu_species_matches[0],
                               te_AN=te_AN_matches[turn],
                               te_function=te_function_matches[turn],
                               te_species=te_species_matches[turn],
                               qu_start=qu_start_matches[turn],
                               qu_end=qu_end_matches[turn],
                               qu_length=qu_length_matches[turn],
                               te_start=te_start_matches[turn],
                               te_end=te_end_matches[turn],
                               te_length=te_length_matches[turn],
                               prob=prob_matches[turn],
                               evalue=evalue_matches[turn],
                               score=score_matches[turn],
                               al_col=al_col_matches[turn],
                               ident=ident_matches[turn],
                               simil=simil_matches[turn],
                               sum_probs=sum_probs_matches[turn],
                               te_neff=te_neff_matches[turn],
                               qu_path=qu_path_matches[0],
                               te_path=te_path_matches[0])
            else:
                HH_name = HHresult(qu_AN=qu_AN_matches[0],
                               qu_function=qu_function_matches[0],
                               qu_species=qu_species_matches[0],
                               te_AN=te_AN_matches[turn],
                               te_function=te_function_matches[turn],
                               te_species=te_species_matches[turn],
                               qu_start=qu_start_matches[turn],
                               qu_end=qu_end_matches[turn],
                               qu_length=qu_length_matches[turn],
                               te_start=te_start_matches[turn],
                               te_end=te_end_matches[turn],
                               te_length=te_length_matches[turn],
                               prob=prob_matches[turn],
                               evalue=evalue_matches[turn],
                               score=score_matches[turn],
                               al_col=al_col_matches[turn],
                               ident=ident_matches[turn],
                               simil=simil_matches[turn],
                               sum_probs=sum_probs_matches[turn],
                               te_neff=te_neff_matches[turn],
                               qu_path=qu_path_matches[turn],
                               te_path=te_path_matches[turn])
            HH_res_list.append(HH_name)

    # Function end
    print('Collected HH_res_list: ' + str(HH_res_list), '\n')
    print('HH_res_list[-1]: ', str(HH_res_list[-1]), '\n')
    return HH_res_list[-1]


def HH_res_list_sort(HH_res_list):
    # used in: HMM_align.py
    '''Organize HHresult instances by e-value and take the best hit for each species'''
    back_align_candidates = []
    HH_res_list.sort(key=operator.attrgetter('evalue'))
    # print('HH_res_list in function: ' + str(HH_res_list))
    species_seen = []
    for HH_res in HH_res_list:
        if HH_res.te_species not in species_seen:
            species_seen.append(HH_res.te_species)
            back_align_candidates.append(HH_res)
    # print('back_align_candidates: ' + str(back_align_candidates))
    return back_align_candidates


def formating_species(species):
    # used in: HMM_align.py
    '''Change format of species name from [species name] to species_name'''
    formated = species
    if ' ' in formated:
        while ' ' in formated:
            formated = formated.replace(' ', '_')
    if '[' in formated:
        while '[' in formated:
            formated = formated.replace('[', '')
    if ']' in formated:
        while ']' in formated:
            formated = formated.replace(']', '')
    return formated
