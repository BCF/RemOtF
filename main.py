# coding: utf-8

#---Function imports---#
import re
import os
import sys
import csv
import uuid
import math
import time
import hjson
import argparse
import tempfile
import pickle
import operator
import subprocess
import logging
import multiprocessing as mproc
from functions import parse_input_fasta, error, check_binaries
from Bio import Entrez

#---Parameters---#
parser = argparse.ArgumentParser()
parser.add_argument(
    "--test-dl-species",
    default=False,
    action="store_true",
    help="Launch the download of the protein of Scerevisae to test the function" +
    "using Entrez")
parser.add_argument(
    '-dl',
    help="Take the name of the species to download, and download all the species's protein from refseq")
args = parser.parse_args()


#---Functions---#
def create_bash_qsub(
        input_seq,
        output_path,
        blastdb_path,
        cpus,
        minimum_ident,
        maximum_ident,
        minimum_coverage,
        maximum_coverage,
        evalue,
        bblastdb,
        name_ortho_by_species,
        copy_path,
        i,
        part):
    '''Creation of a script bash launching multiple_ortholog_searcher and return of the script name'''

    random_folder = str(uuid.uuid4())
    random_folder = str(copy_path + random_folder + '/')
    blast_db_cp = str(blastdb_path + '*')
    bblastdb_cp = str(bblastdb + '*')
    tmp = open(f"temp/remotf.{i}.sh", 'w')
    # Actvate the virtual environnement and move to the folder of RemOtF
    tmp.write("source activate remotf\n")
    tmp.write(f"cd {os.getcwd()}\n")
    # If no copy_path are given, doesn't make copy of the blast db for each
    # node
    if copy_path != '':
        tmp.write(f"mkdir {random_folder}\n")
        tmp.write(f"cp {blast_db_cp} {random_folder}\n")
        tmp.write(f"cp {bblastdb_cp} {random_folder}\n")
        tmp.write(f"python3 multi_ortholog_searcher.py -i {input_seq} \
            -o {output_path} -d {random_folder+blastdb_path.split('/')[-1]}\
            -c {cpus} -f {maximum_ident} -q {minimum_ident}\
            -l {minimum_coverage} -m {maximum_coverage} -e {evalue} -b empty \
            -s {random_folder+bblastdb.split('/')[-1]} --nobs {name_ortho_by_species}")

    else:
        tmp.write(f"python3 multi_ortholog_searcher.py -i {input_seq} \
            -o {output_path} -d {blastdb_path}\
            -c {cpus} -f {maximum_ident} -q {minimum_ident}\
            -l {minimum_coverage} -m {maximum_coverage} -e {evalue} -b empty \
            -s {bblastdb} --nobs {name_ortho_by_species} -p {part}")

    save_temp_name = os.getcwd() + '/' + tmp.name
    tmp.close()
    if copy_path != '':
        return tmp, random_folder
    else:
        return save_temp_name, ''


def divide_fasta(fasta_input, number_nodes):
    ''' Divide the input fasta in multiple fasta of equal length and return the path \
        to the temporary file contening them '''
    # Initialisation value and loading
    j = 0
    list_tempfasta = []
    records = parse_input_fasta(fasta_input)
    # Verify that no node will be used unnecessarily
    if len(records) < number_nodes:
        print("Error, More node are used than fasta sequences are given.")
        sys.exit(1)

    len_file_to_create = math.ceil(len(records) / int(number_nodes))
    for node in range(int(number_nodes)):
        for i in range(len_file_to_create):
            # Creation of the temporaryfile
            if 'tmp' not in locals():
                tmp = tempfile.NamedTemporaryFile(
                    "w", prefix=os.getcwd() + '/temp/', delete=False)
                tmp.write(f'{records[j+i][0]}\n{records[j+i][1]}\n')
            # Save the fasta sequence in the temp file
            else:
                try:
                    tmp.write(f'{records[j+i][0]}\n{records[j+i][1]}\n')
                except IndexError:
                    continue

        tmp.seek(0)
        list_tempfasta.append(tmp.name)
        j += len_file_to_create
        del tmp

    return list_tempfasta


def create_bash_hmm_build(path, number_nodes=0, which_node=0, cpus=2, option='', i=0):
    '''Creation of script lauching HMM_build_align with the given settings
    and return the script name'''
    cwd = os.getcwd()
    tmp = open(f"temp/remotf.{i}.sh", 'w')
    tmp.write("source activate remotf\n")
    tmp.write(f"cd {cwd}\n")
    if option != '--hhdb':
        tmp.write(
            f"python3 HMM_build.py -i {path} --number-nodes {number_nodes}\
            --which-node {which_node} {option}")
    else:
        tmp.write(f"python3 HMM_build.py -i {path} {option} -c {cpus}")

    tmp = cwd + '/' + tmp.name
    return tmp


def create_bash_hmm_align(path, number_nodes=0, which_node=0, cpus = 2, option='', i=0):
    '''Creation of script lauching HMM_build_align with the given settings
    and return the script name'''
    cwd = os.getcwd()
    tmp = open(f"temp/remotf.{i}.sh", 'w')
    tmp.write("source activate remotf\n")
    tmp.write(f"cd {cwd}\n")
    tmp.write(
        f"python3 HMM_align.py -i {path} --number-nodes {number_nodes}\
        --which-node {which_node} {option} -c {cpus}")

    tmp = cwd + '/' + tmp.name
    return tmp


def submit_job(file):
    '''Submit the script on the cluster and return the job id'''

    check = subprocess.Popen(['qsub',
                              '-q',
                              settings['cluster_queue_name'],
                              '-l',
                              f'nodes=1:ppn={settings["cpus"]}',
                              file],
                             stdout=subprocess.PIPE)
    stdout, stder = check.communicate()
    return str(stdout).split('.')[0].split("'")[-1]


def job_finish(id):
    '''Return True if the job is complete'''
    check = subprocess.Popen(['qstat', '-r', id], stdout=subprocess.PIPE)
    stdout = check.communicate()
    try:
        stdout_split = re.split(
            " +", str(stdout[0]).split('"')[1].split("\\n")[-2])[-2]
    except IndexError:
        # If the id is not present (typically because it was complete too long ago)
        # Return True
        return 'job_dispear'
    if stdout_split == "C":
        return True
    else:
        return False


def check_end_job(job_id, end_message):
    ''' Verify the end of computation on all the node launch '''
    while True:
        time.sleep(10)
        end = True
        for job in job_id:
            finish = job_finish(job)
            if not finish:
                end = False
            elif finish == 'job_dispear':
                job_id.remove(job)
        if end:
            print(end_message)
            break


def get_species_taxid(species, email):
    '''Obtention of the Taxonomy Id of species using their name'''
    Entrez.email = email
    for attempt in range(5):
        try:
            species = species.replace(" ", "+").strip()
            search = Entrez.esearch(
                term=species,
                db="taxonomy",
                retmode="xml",
                RetMax=100)
            record = Entrez.read(search)
            if record['Count'] == '0':
                error(f"No taxonomy ID found for {species}")
            search = Entrez.efetch(
                id=record['IdList'][0],
                db="taxonomy",
                retmode="xml")
            data = Entrez.read(search)
            to_return = data[0]["TaxId"]
        except:
            continue
        else:
            break
    '''
    print(f"list of taxid for {species}")
    for d in data:
        print(d["TaxId"], end=' ')
    print("\n")
    '''
    return to_return


def dl_protein_from_taxid(taxid, output, email):
    '''Download the protein of a species using a Taxonomy Id'''

    Entrez.email = email
    for attempt in range(5):
        try:
            entrezQuery = f"refseq[filter] AND txid{taxid}[Organism]"
            searchResultHandle = Entrez.esearch(
                db="protein", term=entrezQuery, RetMax=10000000)
            searchResult = Entrez.read(searchResultHandle)
            searchResultHandle.close()
            uidList = ','.join(searchResult['IdList'])

            out = open(output, "w")
            number_efetch = len(uidList) // 10000
            for i in range(number_efetch):
                # print(10000*(i+1))
                entryData = Entrez.efetch(db="protein",
                                        id=uidList[10000 * i:10000 * (i + 1)],
                                        rettype='fasta').read()
                out.write(entryData)
            if len(uidList) % 10000 > 0:
                entryData = Entrez.efetch(db="protein",
                                        id=uidList[10000 * number_efetch:],
                                        rettype='fasta').read()
                out.write(entryData)
            out.close()
        except:
            continue
        else:
            break

def dl_species_master(select_species):
    '''Download the species from the name and return a list of position to the files'''
    # Obtenition of the taxid id
    taxid_select_species = []
    for s in select_species:
        taxid_select_species.append(get_species_taxid(s, settings["email"]))

    # Download the protein of the selected species
    count = 0
    dl_species_path = []
    print("Start downloading selected protein :")
    for taxid in taxid_select_species:
        print(f"Downloading {select_species[count]} n°{count+1}/" +
              f"{len(taxid_select_species)}")
        dl_protein_from_taxid(taxid, "db/species/" +
                              select_species[count] + '.fa', settings["email"])
        dl_species_path.append("db/species/" + select_species[count] + '.fa')
        count += 1
    print("End of the download")
    return dl_species_path


def load_nb_ortholog_by_species(file):
    '''load the number of ortholog by species stock in a file by main'''
    f = open(file, "r")
    sorted_ortholog_species = []
    for line in f:
        line = line.split(" : ")
        sorted_ortholog_species.append([line[1][:-2], line[0]])
    return sorted_ortholog_species


def selection_interface(sorted_ortholog_species):
    '''Interface to select the species to download'''
    select_species = []
    if len(sorted_ortholog_species) == 0:
        print("No ortholog found")
        return select_species, False

    print("\nHere are the species with which your query presents " +
          "the most orthologs\n")
    for j in range(10):
        print(f"{j}: {sorted_ortholog_species[j][0]} contains : "
              + f"{sorted_ortholog_species[j][1]} orthologs")
    while True:
        try:
            tmp_select_species = input(
                "\nIf you want to use one or more of these species " +
                "as template to compare with your query species with,\n" +
                "please indicate the desired species with their number separated" +
                " with one space (ex : 0 3 5).\n" +
                "If you want to select all of these species please, answer ‘a’ as all.\n" +
                "If you do NOT want to select any of these species, answer ‘n’ as no :\n")
            tmp_select_species = tmp_select_species.split(" ")
            for s in tmp_select_species:
                if s == "n":
                    no_species = True
                elif s == "a":
                    no_species = False
                    for x in range(10):
                        select_species.append(
                            sorted_ortholog_species[int(x)][0])
                else:
                    no_species = False
                    select_species.append(sorted_ortholog_species[int(s)][0])
        except KeyboardInterrupt:
            error("KeyboardInterrupt detected")
        except BaseException:
            select_species = []
            continue
        break
    return select_species, no_species

def rm_duplicate_proteine(filename):
    '''Delete duplicate protein in a fasta'''
    input_files=[filename]
    output_file=str(filename)

    holder=[]
    for file in input_files:
        with open(file,'r') as file:
            rec=file.read().split('>')[1:]
            rec=['>'+i.strip()+'\n' for i in rec]
        holder.extend(rec)

    total='\n'.join(list(set(holder)))

    with open(output_file,'w') as out:
        out.write(total)

def check_completion_previous_interest_proteome(path):
    '''Check if the previous run was complete'''
    if os.path.exists('outputs/'+path+'/bBlast_output/number_ortholog_by_species') \
        and os.path.exists('outputs/'+path+'/back_hmm_library/back_hmm_hhm.ffindex'):
        return True
    else:
        return False

def check_completion_previous_run(path):
    '''Check if the previous run was complete'''
    if os.path.exists('outputs/'+path+'/bBlast_output/number_ortholog_by_species'): 
        return True
    else:
        return False

def reuse_blast_interface():
    '''Interface to select the previous blast run to reuse'''
    global back_hmm_library_reuse 
    reuse_keys = list(reuse.keys())
    # print(keys)
    keys = list(same_blast_dict.keys())
    # print(keys)
    for key in keys:
        interest_proteome = False
        if key not in reuse_keys:
            continue
        if key == first_input_name:
            interest_proteome = True
            back_hmm_library_todo = {}
        if not reuse[key]:
            print(f"{key} find in previous run")
            for i in range(len(same_blast_dict[key])):
                if interest_proteome:
                    if check_completion_previous_interest_proteome(same_blast_dict[key][i]):
                        print(f"{i}/ {os.path.dirname(same_blast_dict[key][i]).replace(settings['input_fasta'][0]+'_', '')}")
                        back_hmm_library_todo[str(i)] = False
                    elif check_completion_previous_run(same_blast_dict[key][i]):
                        print(f"{i}/ {os.path.dirname(same_blast_dict[key][i]).replace(settings['input_fasta'][0]+'_', '')}     <- Need to create the back_hhm_library")
                        back_hmm_library_todo[str(i)] = True
                else:
                    if check_completion_previous_run(same_blast_dict[key][i]):
                        print(f"{i}/ {os.path.dirname(same_blast_dict[key][i]).replace(settings['input_fasta'][0]+'_', '')}")
            while True:
                try:
                    s = input("you can select the run where the blast come with the corresponding number, or write 'n' if you don't want to reuse the blast: ")
                    if s == "n":
                        break
                    else:
                        # print(same_blast_dict[key][int(s)])
                        fd = same_blast_dict[key][int(s)]
                        # print(f"cp -r outputs/{fd} {calculation_path}")
                        os.system(f"cp -r outputs/{fd} {calculation_path}")
                        reuse[key] = True
                        if interest_proteome and not back_hmm_library_todo[s]:
                            back_hmm_library_reuse = True
                        break
                except KeyboardInterrupt:
                    error("KeyboardInterrupt detected")
                except BaseException:
                    continue

#---Main---#
if __name__ == '__main__':

    # Read of the hjson input
    with open("settings.hjson", "r") as s:
        settings = hjson.load(s)

    # Test the dl_species
    if args.test_dl_species:
        print(
            "Starting the downloading in temp/scerevisae.fa, we should expect 6009" +
            " protein from refseq (3 April 2018), the number get is :")
        taxid = get_species_taxid(
            "Saccharomyces cerevisiae",
            settings["email"])
        dl_protein_from_taxid(
            taxid,
            os.getcwd() +
            "/temp/scerevisae.fa",
            settings["email"])
        os.system("egrep '>' temp/scerevisae.fa | wc -l")
        sys.exit(1)

    if args.dl is not None:
        if settings['email'] == '':
            error("Please enter your email in settings.hjson")
        print(f'start downloading {args.dl} in {os.getcwd()}/db/species')
        dl_species_master([args.dl.replace(' ', '_')])
        sys.exit(1)

    # Check binaries
    check_binaries(['blastp', 'blastdbcmd', 'hhmake', 'hhalign', 'mafft'])
    if settings['number_nodes'] != 0:
        check_binaries(['qsub', 'qstat'])


    ############################## INPUT SECURITY ###############################


    if mproc.cpu_count() < settings["cpus"] and settings['number_nodes'] == 0:
        error("The number of cpus is superior to the number available in local")
    if len(settings["input_proteome"]) != len(settings["input_fasta"]):
        error("Input fasta and input_proteome didn't hace the same number of entries, or it should be equal")
    if not os.path.exists(settings['blast_db']+'.phr'):
        error("The BLAST database doesn't exist")
    for i in range(len(settings['input_fasta'])):
        # Verification of input_fasta
        if settings['input_fasta'][i] is None or settings['input_fasta'][i] == '':
            error(f'Input file n°{i+1} is not specified')
        if not os.path.isfile(settings['input_fasta'][i]):
            error(f'Input file n°{i+1} does not exist')
        # Verification of input_proteome
        try:
            if settings['input_proteome'][i] is None or settings['input_proteome'][i] == '':
                error(f'Input proteome n°{i+1} is not specified')
            if not os.path.isfile(settings['input_proteome'][i]):
                error(f'Input proteome n°{i+1} does not exist')
        except IndexError:
            error(f"Input proteome n°{i+1} is needed")
        # Verification of blast_accession and bb_blast_accession
        try:
            if settings['blast_accession'][i] != '' and \
                    not os.path.isfile(settings['blast_accession'][i]):
                error(f"Blast Accession file n°{i+1} doesn't exist")
        except IndexError:
            pass
    if not isinstance(settings['cpus'], int) or settings['cpus'] < 1:
        error('Number of CPUs must be a positive integen')
    try:
        if (settings['minimum_length'] > 200) or (
                settings['minimum_length'] < 0.0):
            error('Minimal length must be in range [0, 200]')
        if (settings['maximum_length'] > 200) or (
                settings['maximum_length'] < 0.0):
            error('Maximal length must be in range [0, 200]')
        if settings['minimum_length'] > settings['maximum_length']:
            error('Minimum length must be smaller than maximum length')
    except TypeError:
        error('minimal_length and maximum_length need to be a number')
    try:
        if (settings['minimum_identity'] > 100) or (
                settings['minimum_identity'] < 0):
            error('Minimal % identity must be in range [0, 100]')
        if (settings['maximum_identity'] > 100) or (
                settings['maximum_identity'] < 0):
            error('Maximal % identity must be in range [0, 100]')
        if settings['minimum_identity'] > settings['maximum_identity']:
            error('Minimum identity must be smaller than maximum identity')
    except TypeError:
        error('minimal_identity and maximum_identity need to be a number')

    if settings['hh_identity'] != 'N/A':
        try:
            float(settings['hh_identity']) 
        except:
            error('hh_identity need to be between 0 and 100 or "N/A"') 
        if float(settings['hh_identity']) < 0  or float(settings['hh_identity']) > 100:
                error('hh_identity need to be between 0 and 100 or "N/A"')

    if settings['number_nodes'] < 0 or not isinstance(
            settings['number_nodes'], int):
        error('Number_nodes must be a positive integers')

    if settings['email'] == '':
        error('email is needed to download additional proteome')

    # If some option are not used, place add empty parameter automatically
    # so the user don't have to do it himself
    if len(settings['input_fasta']) > len(settings['blast_accession']):
        for i in range(len(settings['input_fasta']) -
                       len(settings['blast_accession'])):
            settings['blast_accession'].append('')
    
    ############################ END INPUT SECURITY############################

    startwd = os.getcwd()
    startmaintime = time.time()
    tmp_blast_db_name = []
    lt = time.localtime()
    count_nb_script = 0

    first_input_name = os.path.basename(settings['input_fasta'][0]).replace('.fa', '')
    calculation_path = 'outputs/' + first_input_name + \
        f"_{lt.tm_mday}d_{lt.tm_mon}m_{lt.tm_year}y/"

    # Creation of output directory and save the settings of this launch
    if not os.path.exists(calculation_path):
        os.makedirs(calculation_path)
    if not os.path.exists(calculation_path + 'log'):
        os.makedirs(calculation_path + 'log')
    if not os.path.exists(calculation_path + 'log/settings.hjson'):
        os.system(f"cp settings.hjson {calculation_path + 'log/'}")

    ############################ RE-USE INTERFACE #############################

    # Obtain the list of all subdirs of all previous run
    list_previous_run = []
    dirs = [d for d in os.listdir('outputs/') 
            if os.path.isdir(os.path.join('outputs/', d))]
    for dir in dirs:
        # print(dir)
        subdir = [d for d in os.listdir('outputs/'+dir) if os.path.isdir(os.path.join('outputs/'+dir, d))]
        # print(subdir)
        subdir = [dir+'/'+sd for sd in subdir]
        # print(subdir)
        list_previous_run.append(subdir)
    # sys.exit(1)
    # print(list_previous_run)

    # Search if calculation have already be done previously and propose to
    # reuse the result
    s = settings
    reuse = {}
    reuse_blast_part = {}
    template_db_reuse = False
    query = True
    same_blast_dict = {}
    same_blast_input_list = []
    blast_possible_list = []
    back_hmm_library_reuse = False
    # Check if it as the same blast and input settings
    #blast_db = re.search('# Alias file created: ([^\n]*)', open(s['blast_db']+'.phr', 'r').read()).group(1)
    blast_db = s['blast_db']+'.phr'
    input_basename = [os.path.basename(inp) for inp in s['input_fasta']]
    for inp in input_basename:
        reuse[inp.replace('.fa', '')] = False
        reuse_blast_part[inp.replace('.fa', '')] = 0
    # For each previous run
    for run in list_previous_run:
        # end_dir = dir.split("/")[-1]
        same_blast_settings = False
        same_input = False
        run_name = run[0].split("/")[0]
        #print(run_name)
        r_s = hjson.load(open(f'outputs/{run_name}/log/settings.hjson', 'r'))
        # Check if the blast parameter are the same 
        if r_s['evalue'] == s['evalue'] \
            and r_s['minimum_length'] == s['minimum_length'] \
            and r_s['maximum_length'] == s['maximum_length'] \
            and r_s['minimum_identity'] == s['minimum_identity'] \
            and r_s['maximum_identity'] == s['maximum_identity']:

            #r_blast_db = re.search('# Alias file created: ([^\n]*)', open(r_s['blast_db']+'.pal', 'r').read()).group(1)
            r_blast_db = r_s['blast_db']+'.phr'
            # print(run_name)
            # print(blast_db, r_blast_db)
            if blast_db == r_blast_db:
                same_blast_settings = True

        r_input_basename = [os.path.basename(inp) for inp in r_s['input_fasta']]
        run_basename_species = []
        run_species = []
        for fd in run:
            if fd.endswith('log') or fd.endswith('template_db') or fd.endswith('align') or fd.endswith('back_align'):
                continue
            run_basename_species.append(os.path.basename(fd))
            run_species.append(fd)

        # check if they have the same input
        # print("In same input, ", run_name)
        # print(len(run_basename_species), len(r_input_basename))
        if len(run_basename_species) >= len(r_input_basename):
            if r_input_basename[0] == input_basename[0]:
                same_input = True
                if len(r_input_basename) > 1 and len(input_basename) > 1:
                    for i in input_basename[1:]:
                        # print(i, i in r_input_basename[1:])
                        # print(i not in r_input_basename[1:])
                        # print(r_input_basename[1:])
                        if i not in r_input_basename[1:]:
                            print(' in not same_input')
                            same_input = False
        else: 
            continue
        # print(same_blast_settings, same_input)
    # sys.exit(1)

        if same_blast_settings and same_input:
            # print(f"add {run_name} to the same_blast_input_list")
            same_blast_input_list.append([run_name+'/'+fd for fd in run_basename_species])
            for fd in run_species:
                if os.path.basename(fd) not in list(same_blast_dict.keys()):
                    same_blast_dict[os.path.basename(fd)] = [fd]
                else:
                    same_blast_dict[os.path.basename(fd)].append(fd)
        # check if some of the input are the same for reuse 
        # and append them in same_blast_dict
        elif same_blast_settings and not same_input:
            for fd in run_species:
                if os.path.basename(fd) not in list(same_blast_dict.keys()):
                    same_blast_dict[os.path.basename(fd)] = [fd]
                else:
                    same_blast_dict[os.path.basename(fd)].append(fd)
        else: 
            # print('in else')
            # print([run_name+'/'+d for d in run_basename_species \
                                            # if d != 'log' or d != 'template_db' or d != 'back_align' or d != 'align'])
            blast_possible_list.extend([run_name+'/'+d.replace('.fa', '') for d in run_basename_species \
                                            if d != 'log' or d != 'template_db' or d != 'back_align' or d != 'align'])

        # print('run_basename_species: ', run_basename_species)
        # print(same_blast_settings, same_input)
        # print('same_blast_input_list: ', same_blast_input_list)
        # print("same_blast_dict", same_blast_dict, '\n\n', 'blast_possible_list: ', blast_possible_list)
    # print("same_blast_dict", same_blast_dict)
    print(f"{len(same_blast_input_list)} run find with same input, and blast settings")
    print(f"{len(same_blast_dict)} of the inputs are present with the same blast settings in a previous run ")
    # Possible erreur avec run_basename species
    # print(same_blast_input_list)
    print("Run with the possibility of reuse the back_blast and the template hmm")
    for i in range(len(same_blast_input_list)):
        print(f"{i}/  {os.path.dirname(same_blast_input_list[i][0]).replace(f'{os.path.basename(same_blast_input_list[i][0])}_', '')}:\t{', '.join([os.path.basename(r) for r in same_blast_input_list[i]])}")
    # print(same_blast_input_list)
    while True:
        try:
            selection = input(f"If you want to reuse the back_blast and template_db of one of those run write the corresponding number, or 'n' to select none: ")     
            selection.replace(' ', '')
            # print(selection)
            if selection == "n":
                break
            else:
                '''
                for fd_base in [os.path.basename(fd) for fd in same_blast_input_list[int(selection)]]:
                    # if not os.path.exists(f"{calculation_path}{fd_base}"):
                    #     os.makedirs(f"{calculation_path}{fd_base}")
                    print('ICI')
                    print(same_blast_input_list[int(selection)])                  
                    print(fd)
                    print(os.path.basename(fd))
                    print(fd_base)
                    print(f"cp -r outputs/{run_name}/{fd_base} {calculation_path}")
                    os.system(f"cp -r outputs/{run_name}/{fd_base} {calculation_path}")
                    # print(f"cp -r outputs/{run_name}/{fd_base} {calculation_path}")
                '''
                for fd in same_blast_input_list[int(selection)]:
                    os.system(f"cp -r outputs/{fd} {calculation_path}")
                if not os.path.exists(f"{calculation_path}template_db/"):
                    os.makedirs(f"{calculation_path}template_db/")
                os.system(f"cp outputs/{same_blast_input_list[int(selection)][0].split('/')[0]}/template_db/template_db* {calculation_path}template_db/")
                #print(f"cp outputs/{same_blast_input_list[int(selection)][0].split('/')[0]}/template_db/template_db* {calculation_path}template_db/")
                back_hmm_library_reuse = True
                reuse = {d:True for d in reuse}
                # print(reuse)
                template_db_reuse = True
                break
        except KeyboardInterrupt:
            error("KeyboardInterrupt detected")
        except BaseException:
            continue
    
    if not template_db_reuse:
        reuse_blast_interface()


    ########################### START bBlasting ##############################


    i = 0
    first = True
    l_species_output = []
    # element is not use but i, the purpose is too ensure the for loop will take in count
    # the new species we can select at the end of the calculation
    for element in settings["input_fasta"]:

        # Obtention of the folder present in outputs
        proteome_name = os.path.basename(settings['input_fasta'][i]).replace('.fa', '')
        bBlast_output_path = calculation_path + proteome_name + '/bBlast_output/'
        l_species_output.append(calculation_path + proteome_name + '/')

        # Launch calculation only if we don't use previous result
        if reuse[proteome_name] == False:

            # Creation of the bBlasting directory
            if not os.path.exists(bBlast_output_path):
                os.makedirs(bBlast_output_path)

            # Create automatically the back_blast_db if it doesn't
            # already exist
            bblastdb = startwd + '/db/input_blast_db/' +\
                settings['input_proteome'][i].split('/')[-1]
            if not os.path.exists(bblastdb + '.phd'):
                os.system(
                    f"cp {settings['input_proteome'][i]} db/input_blast_db/")
                rm_duplicate_proteine(bblastdb)
                os.system(
                    f"makeblastdb -dbtype prot -in {bblastdb} -input_type fasta\
                            -parse_seqids -hash_index")
                os.system(f"rm {bblastdb}")

            l_name_ortho_by_species = []

            # Launching multi_ortholog_searcher on several nodes
            # or in local if number_nodes == 0
            if reuse_blast_part[proteome_name] == 0:
                part_blast = 0
                if settings['number_nodes'] == 0:
                    name_ortho_by_species = str(uuid.uuid4())
                    l_name_ortho_by_species.append(name_ortho_by_species)

                    os.system(
                        f"python3 multi_ortholog_searcher.py -i {settings['input_fasta'][i]} \
                                -o {bBlast_output_path} -d {settings['blast_db']} \
                                -c {settings['cpus']} \
                                -f {settings['maximum_identity']} \
                                -q {settings['minimum_identity']} \
                                -l {settings['minimum_length']} \
                                -m {settings['maximum_length']} \
                                -e {settings['evalue']} \
                                -b empty -s {bblastdb} \
                                --nobs {name_ortho_by_species}")


                    # print(f"cp {settings['input_fasta'][i]} {bBlast_output_path}input_{part_blast}.fa")
                    os.system(f"cp {settings['input_fasta'][i]} {bBlast_output_path}input_{part_blast}.fa")
                else:
                    list_fasta_tmp = divide_fasta(settings['input_fasta'][i],
                                                settings['number_nodes'])
                    for tmp_fasta in list_fasta_tmp:

                        # print(f"cp {tmp_fasta} {bBlast_output_path}input_{part_blast}.fa")
                        name_ortho_by_species = str(uuid.uuid4())
                        l_name_ortho_by_species.append(name_ortho_by_species)

                        tmp_bash = create_bash_qsub(
                            tmp_fasta,
                            bBlast_output_path,
                            settings['blast_db'],
                            settings['cpus'],
                            settings['minimum_identity'],
                            settings['maximum_identity'],
                            settings['minimum_length'],
                            settings['maximum_length'],
                            settings['evalue'],
                            bblastdb,
                            name_ortho_by_species,
                            settings['copy_path'],
                            count_nb_script,
                            part_blast)

                        os.system(f"cp {tmp_fasta} {bBlast_output_path}input_{part_blast}.fa")
                        part_blast += 1
                        count_nb_script += 1
                        # Save the name of the temp db for deletion
                        tmp_blast_db_name.append(tmp_bash[1])
                        # Submitting jobs to the cluster queue
                        os.chdir(calculation_path + 'log/')
                        os.system(f'qsub -q {settings["cluster_queue_name"]} \
                                -l nodes=1:ppn={settings["cpus"]} {tmp_bash[0]}')
                        os.chdir(startwd)
            # else:
            #     part_blast = 0
            #     if settings['number_nodes'] == 0:
            #         name_ortho_by_species = str(uuid.uuid4())
            #         l_name_ortho_by_species.append(name_ortho_by_species)

            #         os.system(
            #             f"python3 multi_ortholog_searcher.py -i {bBlast_ output_path}input_{part_blast}.fa \
            #                     -o {bBlast_output_path} -d {settings['blast_db']} \
            #                     -c {settings['cpus']} \
            #                     -f {settings['maximum_identity']} \
            #                     -q {settings['minimum_identity']} \
            #                     -l {settings['minimum_length']} \
            #                     -m {settings['maximum_length']} \
            #                     -e {settings['evalue']} \
            #                     -b empty -s {bblastdb} \
            #                     --blast-xml {bBlast_ output_path}blastp_{part_blast}.xml \
            #                     --nobs {name_ortho_by_species}")
                    # print(f"cp {settings['input_fasta'][i]} {bBlast_output_path}input_{part_blast}.fa")
                    # os.system(f"cp {settings['input_fasta'][i]} {bBlast_output_path}input_{part_blast}.fa")

            # Search for the file contening the number of ortolog by species
            found = 0
            while found < settings['number_nodes']:
                found = 0
                time.sleep(10)
                temp_list_dir = os.listdir(
                    startwd + '/temp/n_ortho_by_species')
                for element in l_name_ortho_by_species:
                    if element in temp_list_dir:
                        found += 1

            # Calculation of the number of ortholog by species
            dict_number_ortholog_by_species = {}
            for temp in l_name_ortho_by_species:
                l = pickle.load(
                    open(
                        startwd +
                        '/temp/n_ortho_by_species/' +
                        temp,
                        "rb"))
                for species in l:
                    if species[0] not in list(
                            dict_number_ortholog_by_species.keys()):
                        dict_number_ortholog_by_species[species[0]] = species[1]
                    else:
                        dict_number_ortholog_by_species[species[0]] += species[1]

            # Sort the result
            sorted_ortholog_species = sorted(
                dict_number_ortholog_by_species.items(),
                key=operator.itemgetter(1),
                reverse=True)
            # Save the result in a file
            with open(startwd + '/' + bBlast_output_path +
                      '/number_ortholog_by_species', 'w') as out:
                for species in sorted_ortholog_species:
                    out.write(f"{species[1]} : {species[0]} \n")

        # Retrieve the species with the most ortholog of the 1st query
        if i == range(len(settings['input_fasta']))[-1] and first == True:

            # Load the number of ortholog by species from file
            first_bBlast_output_path = calculation_path + first_input_name + \
                '/bBlast_output/number_ortholog_by_species'
            sorted_ortholog_species = load_nb_ortholog_by_species(
                first_bBlast_output_path)

            # Selection interface
            select_species, no_species = selection_interface(sorted_ortholog_species)

            # Download the selected species and add them to the query list
            if not no_species:
                # Download the refseq protein of selected species
                dl_species_path = dl_species_master(select_species)
                # Add to the query list
                for sp in dl_species_path:
                    settings["input_fasta"].append(sp)
                    settings["input_proteome"].append(sp)
                    settings["blast_accession"].append('')
                    bsp = os.path.basename(sp).replace('.fa', '')
                    reuse[bsp] = False
                    if bsp not in list(reuse_blast_part.keys()):
                        reuse_blast_part[bsp] = 0
            
            reuse_blast_interface()

            first = False

        # Deletion of temporary file
        if "l_name_ortho_by_species" in locals():
            for temp in l_name_ortho_by_species:
                try:
                    os.remove(f"{startwd}/temp/n_ortho_by_species/{temp}")
                except:
                    pass
        for temp in tmp_blast_db_name:
            if temp != '':
                try:
                    os.rmdir(f"rm -rf {temp}")
                except:
                    pass
        if settings["number_nodes"] > 0:
            if 'list_fasta_tmp' in locals():
                try:
                    for temp in list_fasta_tmp:
                        os.remove(f"{temp}")
                except:
                    pass

        i += 1

#===================END bBlasting=================#
    if not template_db_reuse:
        # Building of HMM libraries from template species ortholog lists.
        count_which_species = 1
        for species_path in l_species_output[1:]:
            if settings['number_nodes'] == 0:
                os.system(f"python3 HMM_build.py -i {species_path} --hmm-template -c {settings['cpus']}")
            else:
                print(
                    f"launching the creation of hmm_template {count_which_species}/{len(l_species_output[1:])}")
                tmp_bash = []
                job_id = []
                # Creation of temporary bash script
                for node in range(int(settings['number_nodes'])):
                    tmp_bash.append(
                        create_bash_hmm_build(
                            species_path,
                            settings['number_nodes'],
                            node,
                            settings['cpus'],
                            "--hmm-template",
                            count_nb_script))
                    count_nb_script += 1
                # Submitting the job on the cluster
                for t in tmp_bash:
                    os.chdir(calculation_path + 'log/')
                    job_id.append(submit_job(t))
                    os.chdir(startwd)
                    time.sleep(1)

                # Waiting the end of the computation
                check_end_job(job_id, "End of creation of hmm template")

            # Creation of hmm orphan in the template species
            if settings['number_nodes'] == 0:
                os.system(
                    f"python3 HMM_build.py -i {species_path} --hmm-orphan --orphan-template -c {settings['cpus']}")
            else:
                print(
                    f"Launching creation of hmm orphans for template species {count_which_species}/{len(l_species_output[1:])}")
                tmp_bash = []
                job_id = []
                # Creation of temporary bash script
                for node in range(int(settings['number_nodes'])):
                    tmp_bash.append(
                        create_bash_hmm_build(
                            species_path,
                            settings['number_nodes'],
                            node,
                            settings['cpus'],
                            "--hmm-orphan --orphan-template",
                            count_nb_script))
                    count_nb_script += 1
                # Submitting the job on the cluster
                for t in tmp_bash:
                    os.chdir(calculation_path + 'log/')
                    job_id.append(submit_job(t))
                    os.chdir(startwd)
                    time.sleep(1)

                # Waiting the end of the computation
                check_end_job(
                    job_id,
                    f'End of creation of hmm orphans for template species {count_which_species}/{len(l_species_output[1:])}')
                count_which_species += 1

        # Building HMMs from query's orphans
        if settings['number_nodes'] == 0:
            os.system(
                f"python3 HMM_build.py -i {l_species_output[0]} --hmm-orphan -c {settings['cpus']}")
        else:
            print("Launching creation of hmm orphans")
            tmp_bash = []
            job_id = []
            # Creation of temporary bash script
            for node in range(int(settings['number_nodes'])):
                tmp_bash.append(
                    create_bash_hmm_build(
                        l_species_output[0],
                        settings['number_nodes'],
                        node,
                        settings['cpus'],
                        "--hmm-orphan",
                        count_nb_script))
                count_nb_script += 1
            # Submitting the job on the cluster
            for t in tmp_bash:
                os.chdir(calculation_path + 'log/')
                job_id.append(submit_job(t))
                os.chdir(startwd)
                time.sleep(1)

            # Waiting the end of the computation
            check_end_job(job_id, "End of creation of hmm orphans")

    ############################## BACK_HMM_LIBRARY Creation ###############################

        # Creation of library of hmm orphan from all the sequence of the query
        if back_hmm_library_reuse == False:
            if not os.path.exists(l_species_output[0] + 'back_hmm_library/'):
                os.makedirs(l_species_output[0] + 'back_hmm_library/')
            # Break the input proteome in fasta contening one sequence
            query_fasta = parse_input_fasta(settings['input_proteome'][0])
            for each_fasta in query_fasta:
                tmp = open(
                    l_species_output[0] +
                    'back_hmm_library/' +
                    re.search(
                        "^>([^ ]+)",
                        each_fasta[0]).group(1) +
                    '.fa',
                    'w')
                tmp.write(f'{each_fasta[0]}\n{each_fasta[1]}')
                tmp.close()

            if settings['number_nodes'] == 0:
                os.system(
                    f"python3 HMM_build.py -i {l_species_output[0]} --hmm-orphan \
                        --back-hmm-library -c {settings['cpus']}")
            else:
                print("Launching the creation of hmm for the back_hmm_library")
                tmp_bash = []
                job_id = []
                # Creation of temporary bash script
                for node in range(int(settings['number_nodes'])):
                    tmp_bash.append(
                        create_bash_hmm_build(
                            l_species_output[0],
                            settings['number_nodes'],
                            node,
                            settings['cpus'],
                            "--hmm-orphan --back-hmm-library",
                            count_nb_script))
                    count_nb_script += 1
                # Submitting the job on the cluster
                for t in tmp_bash:
                    os.chdir(calculation_path + 'log/')
                    job_id.append(submit_job(t))
                    os.chdir(startwd)

                # Waiting the end of the computation
                check_end_job(job_id, "End of creation of back_hmm_library")


    ####################### Creation of the hhsuite database ####################

        # For the back_hhm_library
            print("Launching the creation of the back_hmm_database")
            if settings['number_nodes'] == 0:
                os.system(f"python3 HMM_build.py -i {l_species_output[0]} --hhdb --back-hmm-library -c {settings['cpus']}")
                print("End of creation of the back_hmm_database")
            else:
                job_id = []
                tmp_bash = create_bash_hmm_build(
                    l_species_output[0],
                    cpus = settings['cpus'],
                    option = "--hhdb --back-hmm-library",
                    i = count_nb_script
                )
                os.chdir(calculation_path + 'log/')
                job_id.append(submit_job(tmp_bash))
                os.chdir(startwd)
                count_nb_script += 1
                check_end_job(job_id, "End of creation of the back_hmm_database")

        # Creation of folder for hhdb and moving file
        template_db = calculation_path+'template_db/'
        if not os.path.exists(template_db):
            os.makedirs(template_db)
        for species in l_species_output[1:]:
            os.system(f"mv {species+'bBlast_output/orphans/*.a3m'} {template_db}")
            os.system(f"mv {species+'mafft_output/*.a3m'} {template_db}")
            os.system(f"cp {species+'hmm_models/*.hhm'} {template_db}")
        for file in os.listdir(template_db):
            if file.endswith("_msa.a3m"):
                os.rename(template_db+file, template_db+file.replace('_msa.a3m', '.a3m'))

        print("Launching creation of the template_hmm_database")
        if settings['number_nodes'] == 0:
            os.system(f"python3 HMM_build.py -i {template_db} --hhdb")
            print("End of creation of the template_hmm_database")
        else:
            job_id = []
            tmp_bash = create_bash_hmm_build(
                template_db,
                cpus = settings['cpus'],
                option = "--hhdb",
                i = count_nb_script
            )
            os.chdir(calculation_path + 'log/')
            job_id.append(submit_job(tmp_bash))
            os.chdir(startwd)
            count_nb_script += 1
            check_end_job(job_id, "End of creation of the template_hmm_database")

    #============================ Alignment ===============================#
    if settings['number_nodes'] == 0:
        os.system(
            f"python3 HMM_align.py -i {l_species_output[0]} \
                --align -c {settings['cpus']}")
    else:
        print("Launching hmm alignment")
        tmp_bash = []
        job_id = []
        pool = mproc.Pool()
        # Creation of temporary bash script
        for node in range(int(settings['number_nodes'])):
            tmp_bash.append(
                create_bash_hmm_align(
                    l_species_output[0],
                    settings['number_nodes'],
                    node,
                    settings['cpus'],
                    "--align",
                    count_nb_script))
            count_nb_script += 1
        # Submitting the job on the cluster
        for t in tmp_bash:
            os.chdir(calculation_path + 'log/')
            job_id.append(submit_job(t))
            os.chdir(startwd)
            time.sleep(1)

        # Waiting end of computation on the cluster
        check_end_job(job_id, "End of creation of aligmnent")

    # back_align
    # Creation of the outputs
    with open(calculation_path + 'orthologs.csv', 'w', newline='') as csvfile:
        resultwriter = csv.writer(csvfile, delimiter=',')
        resultwriter.writerow(['Orphan Accession Number',
                                'Orphan Function',
                                'Orphan Species',
                                'Ortholog Accession Number',
                                'Ortholog Function',
                                'Ortholog Species',
                                'Orphan Start', 
                                'Orphan End', 
                                'Orphan Aligned Length', 
                                'Orphan Sequence Length', 
                                'Ortholog Start',
                                'Ortholog End',
                                'Ortholog Aligned Length', 
                                'Ortholog Sequence Length', 
                                'E Value',
                                'Identity',
                                'Similarity'])

    with open(calculation_path + 'homologs.csv', 'w', newline='') as csvfile:
        resultwriter = csv.writer(csvfile, delimiter=',')
        resultwriter.writerow(['Orphan Accession Number',
                                'Orphan Function',
                                'Orphan Species',
                                'Homolog Accession Number',
                                'Homolog Function',
                                'Homolog Species',
                                'Orphan Start', 
                                'Orphan End', 
                                'Orphan Aligned Length', 
                                'Orphan Sequence Length', 
                                'Homolog Start',
                                'Homolog End',
                                'Homolog Aligned Length', 
                                'Homolog Sequence Length', 
                                'E Value',
                                'Identity',
                                'Similarity'])

    if settings['number_nodes'] == 0:
        os.system(
            f"python3 HMM_align.py -i {l_species_output[0]} \
                --back-align --identity {settings['hh_identity']} \
                 -c {settings['cpus']}")
    else:
        print("Launching the back_hmm_alignment")
        tmp_bash = []
        job_id = []
        # Creation of temporary bash script
        for node in range(int(settings['number_nodes'])):
            tmp_bash.append(
                create_bash_hmm_align(
                    l_species_output[0],
                    settings['number_nodes'],
                    node,
                    settings['cpus'],
                    f"--back-align --identity {settings['hh_identity']}",
                    count_nb_script))
            count_nb_script += 1
        # Submitting the job on the cluster
        for t in tmp_bash:
            os.chdir(calculation_path + 'log/')
            job_id.append(submit_job(t))
            os.chdir(startwd)
            time.sleep(1)

        # Not functionnal, needs to be changed
        # Waiting the end of the calculation
        check_end_job(job_id, "End of Back_align")

    print(
        f"End of RemOtF, your run {calculation_path.split('/')[-1]} is finished," +
        f" the result are in the homologs.csv and orthologs.csv stored in {calculation_path}")
