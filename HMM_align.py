import os
import csv
import time
import argparse
import multiprocessing as mproc
from functions import parse_hh_align, formating_species, select_own_file

parser = argparse.ArgumentParser(
    description="Proceed to the creation of hmm and the align depending of the argument given")
parser.add_argument(
    "-i",
    "--input",
    help="Path to the result of the species of interest")
parser.add_argument(
    "-f",
    "--file",
    help="Path to the file contening the list of path of each species")
parser.add_argument("--number-nodes", default=0, help="Number of nodes used")
parser.add_argument(
    "--which-node",
    default=0,
    help="To know which part of the data correspond to its so the node don't redo the same task")
parser.add_argument(
    "--align", default=False, action="store_true", help="proceed to the alignement of\
        the hmm_orphan against the hmm, it need the list of the path to each species folder,\
        it can be load with (-f folder_of_current_computation/log/list_species_output)")
parser.add_argument(
    "--back-hmm-library",
    default=False,
    action="store_true",
    help="Need to be use with --hmm-orphan")
parser.add_argument(
    "--back-align",
    default=False,
    action="store_true",
    help="Get the best result of the align")
parser.add_argument(
    "-e",
    "--evalue",
    default=1e-2,
    help="Maximum Evalue to be consider as a homologs")
parser.add_argument(
    "-c",
    "--cpus",
    type=int,
    default=2,
    help="Number of cpus at disposition")
parser.add_argument(
    "--identity",
    default="N/A",
    help="Minimum pourcentage of identity for the align to be taken as an homolog or an ortholog [0, 100]")
args = parser.parse_args()

#---Functions---#
# These functions' goal is to organize the queries and template and call
# hmm_align() accordingly


def hmm_align_process(orphan):
    global hmm_orphans_path, template_db_path, align_outputs_path
    os.system(f"hhblits -i {hmm_orphans_path+orphan}" +
        f" -d {template_db_path} -o {align_outputs_path+orphan.replace('.hhm', '.hhr')}"+
        " -cpu 1")


def back_align_process(best_hit):
    global l_species_output, back_db_path
    # print(back_te_path)
    interest_protein = best_hit[0].replace('ortho.hhr', '')
    new_base_name = interest_protein + os.path.basename(best_hit[1])
    # print(new_base_name, best_hit[1])
    output_path = str(best_hit[1].replace('template_db', 'back_align')\
        .replace(os.path.basename(best_hit[1]), new_base_name))
    # print("output_path: ", output_path)
    os.system(f"hhblits -i {best_hit[1]}" +
        f" -d {back_db_path} -o {output_path.replace('.hhm', '.hhr')}"+
        " -cpu 1")
    print(f"hhblits -i {best_hit[1]}" +
        f" -d {back_db_path} -o {output_path.replace('.hhm', '.hhr')}"+
        " -cpu 1")


def back_align_parse_process(back_align_file_path):
    return parse_hh_align(back_align_file_path, False)

def extract_file_from_hhdb(id, db_path, output_path):
    """Extract an hhm file from a hhsuitedb to the output_path and return the path of the file"""
    
    hhm_data = open(db_path+'_hhm.ffdata', 'r').read()
    hhm_index = open(db_path+'_hhm.ffindex', 'r').readlines()
    hhm_index = [line.split('\t') for line in hhm_index]

    for line in hhm_index:
        # print(line)
        # print(id.replace('0_', '')+'_ortho.hhm')
        if line[0] == id.replace('>0_', '')+'_ortho.hhm':
            # print('####################################################')
            # print(line)
            output = open(output_path+id.replace('>0_', '')+'_ortho.hhm', 'w')
            print(f"Extracting hhm in {output_path+id.replace('>0_', '')}_ortho.hhm")
            output.write(hhm_data[int(line[1]):int(line[1])+int(line[2].replace('\n', ''))-2])
            output.flush()

    return output_path + id.replace('>0_', '') + "_ortho.hhm"


#---Main---#
# Comparaison
if args.align:
    starttime = time.time()

    # creating paths and gathering input files
    hmm_orphans_path = args.input + 'hmm_orphans/'
    #DECOM os.system(f"rm {hmm_orphans_path}*.a3m")
    hmm_orphans = select_own_file(
        hmm_orphans_path,
        args.number_nodes,
        args.which_node)

    run_path = args.input.split("/")[0] + '/' + \
        args.input.split("/")[1] + '/'
    align_outputs_path = run_path + 'align/'
    template_db_path = run_path + 'template_db/template_db'
    if not os.path.exists(align_outputs_path):
        try:
            os.makedirs(align_outputs_path)
        except FileExistsError:
            pass

    # Calling the function hmm_align_process with all elements of hmm_orphans \
    # to be divided between cores in pool. Pool takes all the cores in the node/CPU \
    # but one free to organize the memory.
    # print(f"hhblits -i {hmm_orphans_path+hmm_orphans[0]}" +
    #     f"-d {template_db_path} -o {align_outputs_path+hmm_orphans[0].replace('.hhm', '.hhr')}"+
    #     "-cpu 1")
    pool = mproc.Pool(args.cpus - 1)
    pool.map(hmm_align_process, hmm_orphans)
    
    # erase a3m files in align_outputs_dir
    os.system('rm ' + align_outputs_path + '*.a3m')
    print('Runtime Align: {} sec'.format(int(time.time() - starttime)))

if args.back_align:

    starttime = time.time()

    # Setup path
    run_path = args.input.split("/")[0] + '/' + \
        args.input.split("/")[1] + '/'
    align_dir_path = run_path + 'align/'
    back_align_dir_path = run_path + 'back_align/'
    back_db_path = args.input + 'back_hmm_library/back_hmm'
    template_db_path = run_path + 'template_db/template_db'
    if not os.path.exists(back_align_dir_path):
        os.makedirs(back_align_dir_path)
    
    # Obtention of align's file path
    align_file_list = select_own_file(
        align_dir_path,
        args.number_nodes,
        args.which_node)
    # print("align_file_list: ", align_file_list)
    
    complete_align_file_list = []
    for file in align_file_list:
        complete_align_file_list.append(align_dir_path+file)
    # print("complete_align_file_list", complete_align_file_list)
    # print('align file: ', complete_align_file_list)

    # Get the best hit of each alignment in format hhr:
    pool = mproc.Pool(args.cpus - 1)
    res = pool.map(parse_hh_align, complete_align_file_list)
    # for r in res:
    #     print("result parse_hh_align:\n", str(r))

    # extraction of hhm file from the template_db
    hhm_best_hit_list = []
    for i in range(len(res)):
        # Extract them only if evalue is under the define one
        # if res[i].evalue < float(args.evalue):
        # if identity == NA/NR or an str just don't use this to filter the hit
        # print("args.identity: ", args.identity)
        # print(" res[i].ident: ",  res[i].ident)
        if args.identity == 'N/A':
            hhm_best_hit_list.append((align_file_list[i],\
            extract_file_from_hhdb(res[i].te_AN,\
            template_db_path, run_path+'template_db/')))
        elif res[i].ident > float(args.identity):
            hhm_best_hit_list.append((align_file_list[i],\
            extract_file_from_hhdb(res[i].te_AN,\
            template_db_path, run_path+'template_db/')))
    print('hhm_best_hit_list', hhm_best_hit_list)

    print('\nback_db_path ', back_db_path)

    # launch back_align
    pool.map(back_align_process, hhm_best_hit_list)

    # Get the path to the alignment for parsing
    back_align_file_list = select_own_file(
        back_align_dir_path,
        args.number_nodes,
        args.which_node)
    back_align_file_path = []
    for f in back_align_file_list:
        back_align_file_path.append(back_align_dir_path + f)
    # print("back_align_file_path: ", back_align_file_path)

    # parse the back alignment
    res_back_align = pool.map(back_align_parse_process, back_align_file_path)

    # Obtention of path of all file of back_align
    l = os.listdir(back_align_dir_path)
    back_align_file_path = []
    for f in l:
        back_align_file_path.append(back_align_dir_path + f)
    
    print(res_back_align)
    print(res)
    
    for r_back_align in res_back_align:
        for r in res:
            #print(r_back_align.te_path)
            #print(r_back_align.te_path + '/' + r_back_align.te_AN.replace('>', '') + '.hmm')
            #print(os.path.basename(r.te_path.replace('_ortho.hhr', '')))
            # if r_back_align.te_path.find(os.path.basename(r.te_path.replace('_ortho.hhr', ''))) != -1:
            print(r_back_align.te_AN.replace('>', '') + ' vs ' + r.qu_AN)
            if r_back_align.te_AN.replace('>', '') == r.qu_AN:
                print('Found a remote ortholog')
                ortholog_output = run_path + 'orthologs.csv'
                with open(ortholog_output, 'a', newline='') as csvfile:
                    resultwriter = csv.writer(csvfile, delimiter=',')
                    resultwriter.writerow([
                        r.qu_AN, r.qu_function, formating_species(r.qu_species),
                        r.te_AN, r.te_function, formating_species(r.te_species),
                        r.qu_start, r.qu_end, r.qu_end-r.qu_start, 
                        r.qu_length,
                        r.te_start, r.te_end, r.te_end-r.te_start, 
                        r.te_length,
                        r.evalue, r.ident, r.simil])
            else:
                print("Found possible homologs")
                homolog_output = run_path + 'homologs.csv'
                with open(homolog_output, 'a', newline='') as csvfile:
                    resultwriter = csv.writer(csvfile, delimiter=',')
                    resultwriter.writerow([
                        r.qu_AN, r.qu_function, formating_species(r.qu_species),
                        r.te_AN.replace('0_', '').replace('>','') , r.te_function,
                        formating_species(r.te_species),
                        r.qu_start, r.qu_end, r.qu_end-r.qu_start, 
                        r.qu_length,
                        r.te_start, r.te_end, r.te_end-r.te_start, 
                        r.te_length,
                        r.evalue, r.ident, r.simil])

    print('Runtime Back Align: {} sec'.format(int(time.time() - starttime)))
