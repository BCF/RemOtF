# coding: utf-8
import os
import time
import argparse
import subprocess
import multiprocessing as mproc
from functions import check_input_fasta, get_input_fasta_first_record, error,\
  select_own_file

parser = argparse.ArgumentParser(
    description="Proceed to the creation of hmm and the align depending of the argument given")
parser.add_argument(
    "-i",
    "--input",
    help="Path to the result of the species of interest")
parser.add_argument("--number-nodes", default=0, help="Number of nodes used")
parser.add_argument(
    "--which-node",
    default=0,
    help="To know which part of the data correspond to its so the node don't redo the same task")
group = parser.add_mutually_exclusive_group()
group.add_argument(
    "--hmm-template",
    default=False,
    action="store_true",
    help="Create the hmm for template species, need the input path to folder of the species (-i)")
group.add_argument(
    "--hmm-orphan",
    default=False,
    action="store_true",
    help="Create the hmm for the query, need the input path to folder of the species (-i)")
group.add_argument(
    "--hhdb",
    default=False,
    action="store_true",
    help="Launch creation of a hhsuite database")
parser.add_argument(
    "--back-hmm-library",
    default=False,
    action="store_true",
    help="Need to be use with --hmm-orphan")
parser.add_argument(
    "--orphan-template",
    default=False,
    action="store_true",
    help="Create HMM from orphan of the templates species, need the option --hmm-orphan")
parser.add_argument(
    "-c",
    "--cpus",
    type=int,
    default=2,
    help="Number of cpus at disposition")
args = parser.parse_args()


# Functions
# These functions' goal is to call functions with multiple arguments
# because we couldn't divide the tasks between the different nodes in one function.
# So each "_process" function will call the same function changing only
# one argument.

def hmm_normal_process(fastafile):

    def hmm_builder(input_dir, input_file, mafft_output_dir, hh_output_dir):
        ''''''
    # Making Hidden Markov Models in 3 steps:
    # 1/ Checking for validity
        ###### DEBUG #######
        destination = input_dir.split('/')[0:2]
        destination.append('log/')
        destination = '/'.join(destination)
        print(destination)
        f = open(destination + input_file, 'w')
        f.close()
        ###### END DEBUG ######
        print('hmm_builder: Processing ' + input_file)
        input_path = str(input_dir) + str(input_file)
        check_input_fasta(input_path)
        seq = get_input_fasta_first_record(input_path)[1]
        len_ = len(seq)
        if len_ > 32767:
            raise ValueError('Protein is too long: {} residues (max. 32767)'.
                            format(len_))
    # 2/ For each ortholog list, align sequences via MAFFT
        mafft_output = mafft_output_dir + str(input_file).replace('.fa', '_msa.fa')
        os.system(
            'mafft --anysymbol --maxiterate 1000 --globalpair ' +
            input_path +
            ' > ' +
            mafft_output)
        if os.path.isfile(mafft_output):
            print('Saved in ' + mafft_output)
            os.system(f"reformat.pl fas a3m '{mafft_output}' .a3m")
        else:
            error('MAFFT output didn\'t write correctly')

    # UNCOMMENT this code when hhsuitedb.py work correctly
    # 3/ Each MSA is then used to make HMMs
        hh_output = hh_output_dir + str(input_file).replace('.fa', '.hhm')
        try:
            stdout, stderr = subprocess.Popen(['hhmake', '-i', mafft_output,
                '-M', 'first', '-o', hh_output],
                stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
        except Exception:
            error("hhmake stderr: ", stderr)

        if os.path.isfile(hh_output):
            print('Saved in ' + hh_output)
        else:
            error("hhmake stderr: ", stderr)
        ##### DEBUG PART 2 ######
        os.system('rm ' + destination + input_file)
        ##### END DEBUG #####

    global mafft_input_path, mafft_output_path, hmm_models_path
    hmm_builder(
        mafft_input_path,
        fastafile,
        mafft_output_path,
        hmm_models_path)


def hmm_orphans_process(fastafile):

    def hmm_builder_orphans(input_dir, input_file, hh_output_dir):
        # used in: HMM_build.py
        '''Creates HMM from one sequence files'''
    # 1/ Checking for validity
        # print('hmm_builder: Processing ' + input_file)
        input_path = str(input_dir) + str(input_file)
        check_input_fasta(input_path)
        seq = get_input_fasta_first_record(input_path)[1]
        len_ = len(seq)
        if len_ > 32767:
            raise ValueError('Protein is too long: {} residues (max. 32767)'.
                            format(len_))
    # 2/ Each orphan is then used to make HMM
        hh_output = str(hh_output_dir) + str(input_file).replace('.fa', '.hhm')
        try:
            stdout, stderr = subprocess.Popen(['hhmake', '-i', input_path,
                '-M', 'first', '-o', hh_output],
                stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
        except Exception:
            error("hhmake stderr: ", stderr)
        # os.system('hhmake -i ' + input_path + ' -M first -o ' + hh_output)
        if os.path.isfile(hh_output):
            print('HMM built')
            print('Saved in ' + hh_output)
        else:
            #error('HHmake didn\'t write ' + hh_output + ' correctly')
            error("hhmake stderr: ", stderr)
        # Convert fasta to a3m for hhdb_build.py
        os.system(f"reformat.pl fas a3m '{input_path}' .a3m")

    global orphans_input_path, hmm_orphans_path
    hmm_builder_orphans(orphans_input_path, fastafile, hmm_orphans_path)


# Building of HMM libraries from template species ortholog lists.
if args.hmm_template:

    species = args.input
    starttime = time.time()

    # creating paths and gathering input files
    species_blast_path = species + 'bBlast_output/'
    mafft_input_path = species_blast_path + 'identified/'
    mafft_output_path = species + 'mafft_output/'
    if not os.path.exists(mafft_output_path):
        try:
            os.makedirs(mafft_output_path)
        except FileExistsError:
            pass
    hmm_models_path = species + 'hmm_models/'
    if not os.path.exists(hmm_models_path):
        try:
            os.makedirs(hmm_models_path)
        except FileExistsError:
            pass
    mafft_input = select_own_file(
        mafft_input_path,
        args.number_nodes,
        args.which_node)

    # Calling the function hmm_normal_process with all elements of mafft_input
    # to be divided between cores in pool. Pool takes all the cores in the node/CPU
    # but one free to organize the memory.
    pool = mproc.Pool(args.cpus - 1)
    pool.map(hmm_normal_process, mafft_input)

    print('Runtime Creation of HMM template: {} sec'.format(int(time.time() - starttime)))


# Building HMMs from query's orphans
if args.hmm_orphan:
            # creating paths and gathering input files
    if args.back_hmm_library:
        hmm_orphans_path = args.input + 'back_hmm_library/'
        orphans_input_path = args.input + 'back_hmm_library/'
    elif args.orphan_template:
        hmm_orphans_path = args.input + 'hmm_models/'
        orphans_input_path = args.input + 'bBlast_output/orphans/'
    else:
        hmm_orphans_path = args.input + 'hmm_orphans/'
        orphans_input_path = args.input + 'bBlast_output/orphans/'

    if not os.path.exists(hmm_orphans_path):
        try:
            os.makedirs(hmm_orphans_path)
        except FileExistsError:
            pass

    starttime = time.time()
    orphans_input = select_own_file(
        orphans_input_path,
        args.number_nodes,
        args.which_node)
    # print(hmm_orphans_path,
    #     orphans_input_path,
    #     orphans_input)

    # Calling the function hmm_orphans_process with all elements of orphans_input
    # to be divided between cores in pool. Pool takes all the cores in the node/CPU
    # but one free to organize the memory.
    pool = mproc.Pool(args.cpus - 1)
    pool.map(hmm_orphans_process, orphans_input)

    print('Runtime Creation HMM orphan: {} sec'.format(int(time.time() - starttime)))

if args.hhdb:
    if args.back_hmm_library:
        folder_path = args.input + 'back_hmm_library/'
        name_db = 'back_hmm'
    else:
        folder_path = args.input
        name_db = 'template_db'

    starttime = time.time()
    # if args.back_hmm_library:
    #     os.system(f"reformat.pl fas a3m '{folder_path}*fa' .a3m")
    os.system(f"python3 hhdb_build.py --a3m {folder_path}*.a3m --hhm {folder_path}*.hhm --cs219 -c {args.cpus - 1} -d {folder_path+name_db}")
    # print(f"python3 hhdb_build.py --a3m {folder_path}*.a3m --hhm {folder_path}*.hhm --cs219 -c {args.cpus - 1} -d {folder_path+name_db}")

    ### IF HHSUITE DB CREATION WORK CORRECTLY uncomment this part and comment the other
    #os.system(f"hhsuitedb.py --ia3m '{folder_path}*.a3m' --ihhm '{folder_path}*.hhm'\
    #    -o {folder_path}{name_db} --cpu {args.cpus-1} --force")
    
    ### With given hhsuite version
    # os.system(f"rename 's/_msa.a3m/.a3m/' {folder_path}*_msa.a3m")
    # os.system(f"hhsuitedb.py --ia3m '{folder_path}*.a3m' \
    #     -o {folder_path}{name_db} --cpu {args.cpus-1} --force")
    # os.system(f"rm {folder_path+name_db}_hhm.ff*")
    # os.system(f"mpirun -np {mproc.cpu_count()} ffindex_apply_mpi {folder_path+name_db}_a3m.ffdata\
    #     {folder_path+name_db}_a3m.ffindex -i {folder_path+name_db}_hhm.ffindex \
    #     -d {folder_path+name_db}_hhm.ffdata -- hhmake -i stdin -o stdout -v 0")
    # os.system(f"ffindex_build -as {folder_path+name_db}_cs219.ffdata {folder_path+name_db}_cs219.ffindex")

    ### Order database for optimization
    # os.system(f"sort -k3 -n {folder_path+name_db}_cs219.ffindex | cut -f1 > temp/sorting.dat")
    # os.system(f"ffindex_order temp/sorting.dat {folder_path+name_db}_hhm.ffdata \
    # {folder_path+name_db}_hhm.ffindex {folder_path+name_db}_hhm_ordered.ffdata \
    # {folder_path+name_db}_hhm_ordered.ffindex")
    # os.system(f"mv {folder_path+name_db}_hhm_ordered.ffindex {folder_path+name_db}_hhm.ffindex")
    # os.system(f"mv {folder_path+name_db}_hhm_ordered.ffdata {folder_path+name_db}_hhm.ffdata")
    # os.system(f"ffindex_order temp/sorting.dat {folder_path+name_db}_a3m.ffdata \
    # {folder_path+name_db}_a3m.ffindex {folder_path+name_db}_a3m_ordered.ffdata \
    # {folder_path+name_db}_a3m_ordered.ffindex")
    # os.system(f"mv {folder_path+name_db}_a3m_ordered.ffindex {folder_path+name_db}_a3m.ffindex")
    # os.system(f"mv {folder_path+name_db}_a3m_ordered.ffdata {folder_path+name_db}_a3m.ffdata")
    
    #DECOM os.system(f"rm {folder_path}*.hhm {folder_path}*.a3m")
    if args.back_hmm_library:
        os.system(f"{folder_path}*.fa")


    # print(f"sort -k3 -n {folder_path+name_db}_cs219.ffindex | cut -f1 > temp/sorting.dat")
    # print(f"ffindex_order temp/sorting.dat {folder_path+name_db}_hhm.ffdata \
    # {folder_path+name_db}_hhm.ffindex {folder_path+name_db}_hhm_ordered.ffdata \
    # {folder_path+name_db}_hhm_ordered.ffindex")
    # print(f"mv {folder_path+name_db}_hhm_ordered.ffindex {folder_path+name_db}_hhm.ffindex")
    # print(f"mv {folder_path+name_db}_hhm_ordered.ffdata {folder_path+name_db}_hhm.ffdata")
    # print(f"ffindex_order temp/sorting.dat {folder_path+name_db}_a3m.ffdata \
    # {folder_path+name_db}_a3m.ffindex {folder_path+name_db}_a3m_ordered.ffdata \
    # {folder_path+name_db}_a3m_ordered.ffindex")
    # print(f"mv {folder_path+name_db}_a3m_ordered.ffindex {folder_path+name_db}_a3m.ffindex")
    # print(f"mv {folder_path+name_db}_a3m_ordered.ffdata {folder_path+name_db}_a3m.ffdata")


    print('Runtime HHsuite Database creation: {} sec'.format(int(time.time() - starttime)))
