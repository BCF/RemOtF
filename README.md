## Setup
Conda is needed to load the environment containing the library use by the pipeline

Downloading the project :
```
git clone git@framagit.org:BCF/RemOtF.git
```

Loading the environment :
```
conda env create --file environment.yml
```

Activate it:
```
source activate remotf
```

RemOtF need Blast+, MAFFT and hhsuite, the first 2 can be install through conda with the next line:
```
conda install -c bioconda -c conda-forge mafft=7.313 blast=2.7.1
```

### hhsuite
As hhsuite is in beta his installation is more difficult.

You need to install hhsuite from source, [here](https://github.com/soedinglab/hh-suite).

/!\ The version 3.0.3 beta present some bug resolvable with the next warning/command :

- "mpirun" is necessary to do a right installation of hhsuite, it's located in the OpenMPI package

- Before compilate hhsuite you need to checkout the submodule git to another branch with the next command, after continue the installation/compilation using the GCC compiler (and not clang):

    ```
    cd lib/ffindex && git pull origin master && git checkout master && cd -
    ```

- Its possible that you have error due to the absence of a file in hhsuite (data/context_data.crf). If it's the case it will be necessary to add it manually, you can download it [here](https://github.com/soedinglab/hh-suite/blob/master/data/context_data.crf). 

## Blast database creation


First Download in fasta format the sequence for the database creation, like those on the  ftp site of NCBI (ftp://ftp.ncbi.nlm.nih.gov/refseq/). I have chose the fungi release for faster test. 

You can download the fasta sequence from an entire kingdom with the following command where -P correspond to the directory where the data are download: 
```
wget ftp://ftp.ncbi.nlm.nih.gov/refseq/release/fungi/ -r -nd --no-parent -A "*protein.faa.gz" -P ~/RemOtF/db/fungi_db/
```
After that you need to unzip the fasta sequence 
```
cd ~/RemOtF/db/fungi_db/
gunzip *
```
Then if you have more than one fasta file you need to concatenate them in one file like that :
```
cat * > fungi.protein.faa
```
Now we can create the blast database using the makeblastdb program from the Blast+ suite
like this:
```
makeblastdb -dbtype prot -in fungi.protein.faa -input_type fasta -parse_seqids 
```
