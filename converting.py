# coding: utf-8

import csv
import sys

path = str(sys.argv[1])

l_AN = []
l_Function = []
l_Species =[]
l_Length = []
l1_AN = []
l2_AN = []
l_Relationship = []
l_evalue = []

with open(path + 'orthologs.csv', 'r') as ortho_file:
	ortho_reader = csv.reader(ortho_file, delimiter = ',')
	for row in ortho_reader:
		if row[0] != 'Orphan Accession Number':
			if row[0] not in l_AN:
				l_AN.append(row[0])
				l_Function.append(row[1])
				l_Species.append(row[2])
				l_Length.append(row[9])
			if row[3] not in l_AN:
				l_AN.append(row[3])
				l_Function.append(row[4])
				l_Species.append(row[5])
				l_Length.append(row[13])
			l1_AN.append(row[0])
			l2_AN.append(row[3])
			l_Relationship.append('orthologs')
			l_evalue.append(row[14])
ortho_file.close()

with open(path + 'homologs.csv', 'r') as homo_file:
	homo_reader = csv.reader(homo_file, delimiter = ',')
	for row in homo_reader:
		if row[0] != 'Orphan Accession Number':
			if row[0] not in l_AN:
				l_AN.append(row[0])
				l_Function.append(row[1])
				l_Species.append(row[2])
				l_Length.append(row[9])
			if row[3] not in l_AN:
				l_AN.append(row[3])
				l_Function.append(row[4])
				l_Species.append(row[5])
				l_Length.append(row[13])
			l1_AN.append(row[0])
			l2_AN.append(row[3])
			l_Relationship.append('homologs')
			l_evalue.append(row[14])
homo_file.close()

with open(path + 'nodes.csv', 'w', newline = '') as node_file:
	node_writer = csv.writer(node_file, delimiter = ',')
	node_writer.writerow(['id',
							'Function',
							'Species',
							'Length'])
	for node_count in range(len(l_AN)):
		node_writer.writerow([l_AN[node_count],
							l_Function[node_count],
							l_Species[node_count],
							l_Length[node_count]])
node_file.close()

with open(path + 'edges.csv', 'w', newline = '') as edge_file:
	edge_writer = csv.writer(edge_file, delimiter = ',')
	edge_writer.writerow(['from',
							'to',
							'type',
							'weight'])
	for edge_count in range(len(l1_AN)):
		edge_writer.writerow([l1_AN[edge_count],
							l2_AN[edge_count],
							l_Relationship[edge_count],
							l_evalue[edge_count]])
edge_file.close()